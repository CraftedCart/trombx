trombx.event
============

.. contents:: :local:

Description
-----------

Game events that modcharts can hook in to

API reference
-------------

.. lua:automodule:: trombx.event
