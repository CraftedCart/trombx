trombx.notefield.mod.core
=========================

.. contents:: :local:

Description
-----------

Various common structs passed around to notefield modifiers

API reference
-------------

.. lua:automodule:: trombx.notefield.mod.core
