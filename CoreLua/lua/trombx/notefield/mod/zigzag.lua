local mod_core = require("trombx.notefield.mod.core")
local imgui = require("trombx.imgui")

local abs = math.abs
local floor = math.floor

--- @class trombx.notefield.mod.zigzag
local M = {}

-- Gives you the Y position for a triangle wave, for a given X position
-- Values range from 0 - 1
local function triangle_wave(x, period)
  local x_div_period = x / period
  return 2 * abs((x_div_period) - floor(x_div_period + 0.5))
end

--- @class ModZigzag : NoteModBase
--- @field public magnitude number Higher number means larger zig-zags
--- @field public period number How frequently we should zig-zag, in beats. Smaller number means more frequent zig-zags.
--- @field public axis string The axis to zigzag along - should be ``"x"``, ``"y"``, or ``"z"`` - defaults to ``"y"`` if nil
M.ModZigzag = setmetatable({}, mod_core.NoteModBase)
local ModZigzag_metatable = {
  __index = M.ModZigzag,
  __tostring = function(self)
    return string.format("<ModZigzag magnitude=%f period=%f axis=%s>", self.magnitude, self.period, self.axis)
  end,
}

--- Create a :lua:class:`ModZigzag` note modifier
---
--- @param magnitude number|nil Higher number means larger zig-zags. 20 if ``nil``.
--- @param period number|nil How frequently we should zig-zag, in beats. Smaller number means more frequent zig-zags. 0.5 if ``nil``.
--- @param axis string|nil The axis to zigzag along - should be ``"x"``, ``"y"``, or ``"z"``. ``"y"`` if nil.
---
--- @return ModZigzag
function M.ModZigzag.new(magnitude, period, axis)
  return setmetatable(
    {
      magnitude = magnitude or 20,
      period = period or 0.5,
      axis = axis or "y",
    },
    ModZigzag_metatable
    )
end

--- @param ctx NoteModContext
function M.ModZigzag:modify_note(ctx)
  local period = self.period
  local axis = self.axis
  local offset = (triangle_wave(ctx.beats_until_receptor - (period * 0.25), period) - 0.5) * self.magnitude
  ctx.position[axis] = ctx.position[axis] + offset
end

function M.ModZigzag:is_imgui_inspectable()
  return true
end

function M.ModZigzag:imgui_inspect()
  self.magnitude = imgui.drag_float("Magnitude", self.magnitude, 0.1)
  self.period = imgui.drag_float("Period", self.period, 0.1)
  -- TODO: Axis
end

return M
