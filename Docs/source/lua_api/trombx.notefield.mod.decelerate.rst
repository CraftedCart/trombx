trombx.notefield.mod.decelerate
===============================

.. contents:: :local:

Description
-----------

Notes will move faster the further away they are from the receptor - decelerating to the regular scroll speed as they
approach the receptor.

API reference
-------------

.. lua:automodule:: trombx.notefield.mod.decelerate
