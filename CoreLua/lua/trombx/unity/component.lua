local unity_object = require("trombx.unity.unity_object")
local storage = require("trombx_private.interop.storage")
local convert = require("trombx_private.interop.convert")

luanet.load_assembly("UnityEngine.CoreModule")
local UnityEngine = luanet.namespace("UnityEngine")

local UComponent = UnityEngine.Component

local game_object_module
--- Get the ``game_object`` module
---
--- Loaded on-demand to break require cyclical dependencies
local function game_object()
  if game_object_module == nil then
    game_object_module = require("trombx.unity.game_object")
  end
  return game_object_module
end

--- @class trombx.unity.component
local M = {}

--- A component on a GameObject
---
--- @class Component: UnityObject
M.Component = setmetatable({}, { __index = unity_object.UnityObject })
convert.wrapper_to_unity[M.Component] = UComponent

--- @param lua_object Component
--- @return UComponent
local function unwrap(lua_object)
  return storage.unwrap(lua_object) --[[@as UComponent]]
end

--- Get the game object that the component is attached to
---
--- @return GameObject
function M.Component:get_game_object()
  local unity_go = unwrap(self).gameObject
  return storage.get_or_make_wrapped(unity_go, game_object().GameObject)
end

return M
