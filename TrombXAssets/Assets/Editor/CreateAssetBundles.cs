using UnityEditor;
using System.IO;

public class CreateAssetBundles
{
    [MenuItem("TrombX/Build AssetBundles")]
    static void BuildAllAssetBundles()
    {
        string assetBundleDirectory = "Assets/AssetBundles";
        if (!Directory.Exists(assetBundleDirectory))
        {
            Directory.CreateDirectory(assetBundleDirectory);
        }

        // TODO: Build for Mac too when we have TrombX working on Mac (should mostly be a case of getting a LuaJIT
        // dylib built)
        BuildPipeline.BuildAssetBundles(
            assetBundleDirectory,
            BuildAssetBundleOptions.None,
            BuildTarget.StandaloneWindows
            );
    }
}
