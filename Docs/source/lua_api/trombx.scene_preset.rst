trombx.scene_preset
===================

.. contents:: :local:

Description
-----------

Automatically disable some in-game models, the notefield, lighting, cameras, etc. and gives you a near-blank slate to
start adding your own content into the scene.

API reference
-------------

.. lua:automodule:: trombx.scene_preset
