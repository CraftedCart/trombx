local interop = require("trombx_private.interop")
local storage = require("trombx_private.interop.storage")
local convert = require("trombx_private.interop.convert")

luanet.load_assembly("UnityEngine.CoreModule")
local UnityEngine = luanet.namespace("UnityEngine")

local UObject = UnityEngine.Object

--- @class trombx.unity.unity_object
local M = {}

--- Base class for Unity objects
---
--- @class UnityObject
M.UnityObject = {}
convert.wrapper_to_unity[M.UnityObject] = UObject

--- Does this refer to a valid Unity Object?
---
--- Will return ``false`` after the object is destroyed.
---
--- When writing code for Unity C#, it's typical to check if an object is equal to ``null`` to see if it's still alive.
--- Unity will appear to null-out objects once they're destroyed, via the magic of overloading rhe ``==`` operator.
--- Doing the same in Lua however, checking if it's equal to ``nil``, is not possible.
---
--- To properly check if something is a valid Unity object or not, use this...
---
--- .. code-block:: lua
---
---    some_object ~= nil and some_object:is_valid()
---
--- @return boolean
function M.UnityObject:is_valid()
  return interop.is_unity_object_valid(storage.unwrap(self))
end

--- Clone an object
---
--- TODO: Support passing in transforms/positions/rotations
---
--- **Example spawning in a prefab from an asset bundle**
---
--- .. code-block:: lua
---
---    local unity = require("trombx.unity")
---
---    local assets = unity.AssetBundle.load("modchartassets")
---    local tree_game_object = assets:load_game_object("Tree"):instantiate()
---
--- @return UnityObject
function M.UnityObject:instantiate()
  local ty = getmetatable(self).__index
  return storage.get_or_make_wrapped(UObject.Instantiate(storage.unwrap(self)), ty)
end

--- Remove an object at the end of the current update loop, or after ``delay`` seconds
---
--- See https://docs.unity3d.com/ScriptReference/Object.Destroy.html
---
--- @param delay number|nil Defaults to 0
function M.UnityObject:destroy(delay)
  UObject.Destroy(storage.unwrap(self), delay or 0)
end

return M

