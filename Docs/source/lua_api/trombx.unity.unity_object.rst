trombx.unity.unity_object
=========================

.. contents:: :local:

Description
-----------

Base class for Unity objects

API reference
-------------

.. lua:automodule:: trombx.unity.unity_object
