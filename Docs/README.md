TrombX Docs
===========

## Building

If you want to build the docs locally, first make sure you have Python 3 installed.

1. Create a Python virtual environment (`python3 -m venv venv`)
2. Install dependencies (`pip3 install -r requirements.txt`)
3. Build HTML docs (`make html`)

## Style guide

- reStructuredText files should wrap lines at `120` characters
- reStructuredText file names should use `snake_case`
