local test = require("trombx.test")
local interop = require("trombx_private.interop")

local TrombX = CLRPackage("TrombX", "TrombX")

--- @class trombx.note
local M = {}

--- A single note
---
--- @class Note
--- @field public beat_start number The beat on which the note starts on
--- @field public duration number How long the note is, in beat
--- @field public pitch_start number The pitch the note starts at
--- @field public pitch_delta number How much the note pitch bends (almost always ``pitch_end - pitch_start``)
--- @field public pitch_end number The pitch the note ends at
M.Note = {}
local Note_metatable = {
  __index = M.Note,
  __tostring = function(self)
    return string.format(
      "<Note %f, %f, %f, %f, %f>",
      self.beat_start,
      self.duration,
      self.pitch_start,
      self.pitch_delta,
      self.pitch_end
      )
  end,
}

--- Create a new :lua:class:`Note`
---
--- @param beat_start number The beat on which the note starts on
--- @param duration number How long the note is, in beats
--- @param pitch_start number The pitch the note starts at
--- @param pitch_delta number How much the note pitch bends (almost always ``pitch_end - pitch_start``)
--- @param pitch_end number The pitch the note ends at
---
--- @return Note
function M.Note.new(beat_start, duration, pitch_start, pitch_delta, pitch_end)
  return setmetatable(
    {
      beat_start = beat_start,
      duration = duration,
      pitch_start = pitch_start,
      pitch_delta = pitch_delta,
      pitch_end = pitch_end,
    },
    Note_metatable
    )
end

--- Does the note overlap the given time range?
---
--- @param start_beat number Lower bound (inclusive)
--- @param end_beat number Upper bound (exclusive)
---
--- @return boolean
function M.Note:is_in_range(start_beat, end_beat)
  local note_start = self.beat_start
  local note_end = note_start + self.duration
  return not ((note_start < start_beat and note_end < start_beat) or (note_start >= end_beat and note_end >= end_beat))
end

--- A collection of notes
---
--- @class NoteData
--- @field public notes Note[] List of notes - should be in-order
M.NoteData = {}
local NoteData_metatable = {
  __index = M.NoteData,
  __tostring = function(self)
    return string.format("<NoteData #notes = %d>", #self.notes)
  end,
}

--- Create an empty :lua:class:`NoteData` instance
---
--- @return NoteData
function M.NoteData.new()
  return setmetatable(
    {
      notes = {},
    },
    NoteData_metatable
    )
end

--- Get all notes in the currently playing chart
---
--- @return NoteData
function M.NoteData.from_current_song()
  local custom_track_data = TrombX.ScriptEngineBridge.GetCurrentModTrackData()

  -- This should always return a valid object (since we shouldn't even be running scripts if the current track isn't a
  -- mod track), but let's assert it just in case
  assert(interop.is_valid(custom_track_data), "TrombX.ScriptEngineBridge.GetCurrentModTrackData() returned nil")

  local out = M.NoteData.new()

  -- Convert note data from an array of arrays of floats into a NoteData object containing Notes here
  for i = 0, custom_track_data.notes:GetUpperBound(0) do
    local orig_note = custom_track_data.notes[i]
    table.insert(
      out.notes,
      M.Note.new(
        orig_note[0],
        orig_note[1],
        orig_note[2],
        orig_note[3],
        orig_note[4]
        )
      )
  end

  return out
end

--- Remove notes that match the given predicate
---
--- This modifies the :lua:class:`NoteData` in-place, and returns itself
---
--- **Example usage - removing notes whose pitch is more than halfway up the notefield**
---
--- .. code-block:: lua
---
---    local note_data = NoteData.from_current_song()
---      :reject_inplace(function(note) return note.pitch_start > 0 end)
---
--- **Example usage - removing notes that are between beats 4 and 8**
---
--- .. code-block:: lua
---
---    local note_data = NoteData.from_current_song()
---      :reject_inplace(Note.is_in_range, 4, 8)
---
--- @param predicate fun(n:Note,...):boolean Function called for each note in the collection. Return ``true`` to remove this note, ``false``  to keep it.
--- @param ... any Extra args passed to the given predicate
---
--- @return NoteData # self
function M.NoteData:reject_inplace(predicate, ...)
  local i = 1
  local note = self.notes[i]
  while note ~= nil do
    if predicate(note, ...) then
      -- Remove this note
      table.remove(self.notes, i)
    else
      -- Keep this note
      i = i + 1
    end

    note = self.notes[i]
  end

  return self
end

--- Keep notes that match the given predicate, removing all others
---
--- This modifies the :lua:class:`NoteData` in-place, and returns itself
---
--- **Example usage - keeping notes whose pitch is more than halfway up the notefield**
---
--- .. code-block:: lua
---
---    local note_data = NoteData.from_current_song()
---      :filter_inplace(function(note) return note.pitch_start > 0 end)
---
--- **Example usage - keeping notes that are between beats 4 and 8**
---
--- .. code-block:: lua
---
---    local note_data = NoteData.from_current_song()
---      :filter_inplace(Note.is_in_range, 4, 8)
---
--- @param predicate fun(n:Note,...):boolean Function called for each note in the collection. Return ``true`` to keep this note, ``false`` to remove it.
--- @param ... any Extra args passed to the given predicate
---
--- @return NoteData # self
function M.NoteData:filter_inplace(predicate, ...)
  local i = 1
  local note = self.notes[i]
  while note ~= nil do
    if predicate(note, ...) then
      -- Keep this note
      i = i + 1
    else
      -- Remove this note
      table.remove(self.notes, i)
    end

    note = self.notes[i]
  end

  return self
end

-- TODO: Show an example of two notefields, scrolling in opposite directions, where the upper half scrolls one way and
--       the lower half scrolls the other way in the above examples maybe?

--- Iterate over all notes between ``start_beat`` and ``end_beat``
---
--- Usage:
---
--- .. code-block:: lua
---
---    local note_data = NoteData.from_current_song()
---    for i, note in note_data:iter_notes_in_range(10, 20) do
---      print(note)
---    end
---
--- @param start_beat number Lower bound (inclusive)
--- @param end_beat number Upper bound (exclusive)
---
--- @return fun():integer|nil,Note|nil # A function that returns either an index and a :lua:class:`Note` object, or ``nil`` if finished iterating
function M.NoteData:iter_notes_in_range(start_beat, end_beat)
  -- TODO: Binary search for the first note?

  local pos = 1
  local found_notes = false
  return function()
    while pos <= #self.notes do
      local note = self.notes[pos]
      if note:is_in_range(start_beat, end_beat) then
        local current_pos = pos
        pos = pos + 1
        found_notes = true
        return current_pos, note
      elseif not found_notes then
        pos = pos +  1
      else
        -- Done iterating here
        return nil
      end
    end

    -- Reached the end of the notes list
    return nil
  end
end

test.define("trombx.note.Note.is_in_range", function()
  local note = M.Note.new(3, 1, 0, 0, 0) -- Starts at beat 3, has a duration of 1 beat, and all pitch info is zero

  assert(note:is_in_range(3, 20))
  assert(note:is_in_range(3.2, 3.5))
  assert(note:is_in_range(4, 20), "Should be inclusive at start_beat")
  assert(not note:is_in_range(2, 3), "Should be exclusive at end_beat")
  assert(not note:is_in_range(1, 3), "Should be exclusive at end_beat")
  assert(not note:is_in_range(1, 1))
  assert(not note:is_in_range(4.1, 8))
  assert(not note:is_in_range(8, 16))
end)

test.define("trombx.note.NoteData.iter_notes_in_range", function()
  local note_data = M.NoteData.new()

  local note1 = M.Note.new(1, 1, 0, 0, 0)
  local note2 = M.Note.new(2, 1, 0, 0, 0)
  local note3 = M.Note.new(3, 1, 0, 0, 0)
  local note4 = M.Note.new(4, 1, 0, 0, 0)
  local note5 = M.Note.new(5, 1, 0, 0, 0)
  table.insert(note_data.notes, note1)
  table.insert(note_data.notes, note2)
  table.insert(note_data.notes, note3)
  table.insert(note_data.notes, note4)
  table.insert(note_data.notes, note5)

  local iter = note_data:iter_notes_in_range(3, 4)

  local iter_note2_index, iter_note2 = iter()
  test.assert_eq(iter_note2_index, 2)
  test.assert_eq(iter_note2, note2)

  local iter_note3_index, iter_note3 = iter()
  test.assert_eq(iter_note3_index, 3)
  test.assert_eq(iter_note3, note3)

  -- end_beat is exclusive so this fourth note shouldn't show up
  -- local iter_note4_index, iter_note4 = iter()
  -- test.assert_eq(iter_note4_index, 4)
  -- test.assert_eq(iter_note4, note4)

  local iter_end = iter()
  test.assert_nil(iter_end)
end)

-- Properly testing from_current_song would require loading a specific chart - for now, let's just make sure it doesn't
-- raise an error at least
test.define("trombx.note.NoteData.from_current_song_doesnt_error", function()
  M.NoteData.from_current_song()
end)

return M
