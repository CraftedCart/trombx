﻿namespace KeraLua
{
    /// <summary>
    /// Enum for pseudo-index used by registry table
    /// </summary>
#pragma warning disable CA1008 // Enums should have zero value
    public enum LuaRegistry
#pragma warning restore CA1008 // Enums should have zero value
    {
        /// <summary>
        /// pseudo-index used by registry table
        /// </summary>
        Index = -10000, // LUA_REGISTRYINDEX

        /// <summary>
        /// pseudo-index used by environ table
        /// </summary>
        EnvironIndex = -10002, // LUA_ENVIRONINDEX

        /// <summary>
        /// pseudo-index used by global table
        /// </summary>
        GlobalsIndex = -10002, // LUA_GLOBALSINDEX
    }
}
