trombx.unity.camera
===================

.. contents:: :local:

Description
-----------

Renders the world

API reference
-------------

.. lua:automodule:: trombx.unity.camera
