﻿Shader "TrombX/NotefieldX/NoteLineShader"
{
    Properties {}

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
        }

        Blend SrcAlpha OneMinusSrcAlpha
        Cull Off // Show front and back faces

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                fixed4 color : COLOR;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                fixed4 color : COLOR;
            };

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                o.color = v.color;
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                fixed4 innerCol = i.color;
                fixed4 outerCol = fixed4(1, 1, 1, 1);

                fixed4 col;
                if (i.uv.y > 0.25 && i.uv.y < 0.75)
                {
                    col = innerCol;
                }
                else
                {
                    col = outerCol;
                }

                return col;
            }
            ENDCG
        }
    }
}
