﻿using System;
using BepInEx;
using BaboonAPI.Hooks.Initializer;
using BaboonAPI.Hooks.Tracks;
using BepInEx.Bootstrap;
using BepInEx.Logging;
using FallenWorld;
using HarmonyLib;
using UnityEngine;
using UnityEngine.Rendering;

namespace TrombX
{
    [BepInPlugin("craftedcart.tromb_x", "TrombX", "0.1.0.0")]
    [BepInDependency("ch.offbeatwit.baboonapi.plugin")]
    [BepInDependency("TrombLoader")]
    [BepInDependency("TootTallyCore", BepInDependency.DependencyFlags.SoftDependency)] // For patching - TODO remove in future?
    public class TrombXPlugin : BaseUnityPlugin
    {
        private static TrombXPlugin? _instance;
        internal static TrombXPlugin Instance
        {
            get
            {
                if (_instance is null)
                {
                    throw new NullReferenceException("Instance not initialized - accessing too early?");
                }

                return _instance;
            }
        }

        internal new ManualLogSource Logger => base.Logger;

        private readonly Harmony _harmony = new("craftedcart.tromb_x");

        private void Awake()
        {
            _instance = this;
            GameInitializationEvent.Register(Info, TryInitialize);
            TrackRegistrationEvent.EVENT.Register(new TrackLoader());
        }

        private void TryInitialize()
        {
            foreach (var plugin in Chainloader.PluginInfos)
            {
                if (plugin.Value.Metadata.GUID == "TootTallyCore")
                {
                    Logger.LogInfo("Found TootTallyCore, patching it!");
                    _harmony.PatchAll(typeof(TootTallyCorePatch));
                    break;
                }
            }

            _harmony.PatchAll(typeof(GameControllerPatch));
        }
    }
}
