using System;
using System.Collections.Generic;
using NLua;

namespace TrombX
{
    /// <summary>
    /// We cannot use regular C# events since assembly generation doesn't seem to work (which NLua does when trying to
    /// add a callback to a regular CLR event)
    ///
    /// So we use this instead
    /// </summary>
    public class ScriptEvent
    {
        private enum ActionType
        {
            Subscribe,
            Unsubscribe,
        }

        private struct Action
        {
            internal ActionType Type;
            internal LuaFunction Func;
        }

        private readonly List<LuaFunction> _subscribers = new();

        /// <summary>
        /// Defer binding/unbinding so we don't get concurrent modification exceptions when binding/unbinding while we
        /// are iterating over calling all subscribers
        /// </summary>
        private readonly List<Action> _pendingActions = new();

        public void Bind(LuaFunction func)
        {
            _pendingActions.Add(new Action { Type = ActionType.Subscribe, Func = func });
        }

        public void Unbind(LuaFunction func)
        {
            _pendingActions.Add(new Action { Type = ActionType.Unsubscribe, Func = func });
        }

        internal void Invoke()
        {
            foreach (var action in _pendingActions)
            {
                switch (action.Type)
                {
                    case ActionType.Subscribe:
                        _subscribers.Add(action.Func);
                        break;
                    case ActionType.Unsubscribe:
                        _subscribers.RemoveAll((item) => Equals(item, action.Func));
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            _pendingActions.Clear();

            foreach (var func in _subscribers)
            {
                func.Call();
            }
        }

        /// <summary>
        /// Clear all subscribers
        /// </summary>
        internal void Clear()
        {
            _subscribers.Clear();
            _pendingActions.Clear();
        }
    }
}
