local component = require("trombx.unity.component")
local mathx = require("trombx.mathx")
local storage = require("trombx_private.interop.storage")
local convert = require("trombx_private.interop.convert")

luanet.load_assembly("UnityEngine.CoreModule")
local UnityEngine = luanet.namespace("UnityEngine")
local TrombX = CLRPackage("TrombX", "TrombX")

local Vec3 = mathx.Vec3

local UTransform = UnityEngine.Transform
local UTransform_GetLocalPosition = TrombX.ScriptEngineBridge.Transform_GetLocalPosition
local UTransform_SetLocalPosition = TrombX.ScriptEngineBridge.Transform_SetLocalPosition
local UTransform_GetLocalEulerAngles = TrombX.ScriptEngineBridge.Transform_GetLocalEulerAngles
local UTransform_SetLocalEulerAngles = TrombX.ScriptEngineBridge.Transform_SetLocalEulerAngles
local UTransform_GetLocalScale = TrombX.ScriptEngineBridge.Transform_GetLocalScale
local UTransform_SetLocalScale = TrombX.ScriptEngineBridge.Transform_SetLocalScale
local UTransform_LookAt = TrombX.ScriptEngineBridge.Transform_LookAt

--- @class trombx.unity.object_transform
local M = {}

--- A GameObject's transform
---
--- @class ObjectTransform: Component
M.ObjectTransform = setmetatable({}, { __index = component.Component })
convert.wrapper_to_unity[M.ObjectTransform] = UTransform

--- @param lua_object ObjectTransform
--- @return UTransform
local function unwrap(lua_object)
  return storage.unwrap(lua_object) --[[@as UTransform]]
end

--- Get the local position
---
--- See https://docs.unity3d.com/ScriptReference/Transform-localPosition.html
---
--- @return Vec3
function M.ObjectTransform:get_local_position()
  local x, y, z = UTransform_GetLocalPosition(unwrap(self))
  return mathx.Vec3.new(x, y, z)
end

--- Set the local position
---
--- See https://docs.unity3d.com/ScriptReference/Transform-localPosition.html
---
--- @param position Vec3 The new local position
function M.ObjectTransform:set_local_position(position)
  UTransform_SetLocalPosition(unwrap(self), position.x, position.y, position.z)
end

--- Get the local rotation, in degrees for X/Y/Z axes
---
--- Objects are rotated around the Z, then X, then Y axis in Unity.
---
--- See https://docs.unity3d.com/ScriptReference/Transform-localEulerAngles.html
---
--- @return Vec3
function M.ObjectTransform:get_local_euler_angles()
  local x, y, z = UTransform_GetLocalEulerAngles(unwrap(self))
  return mathx.Vec3.new(x, y, z)
end

--- Set the local rotation, in degrees for X/Y/Z axes
---
--- Objects are rotated around the Z, then X, then Y axis in Unity.
---
--- See https://docs.unity3d.com/ScriptReference/Transform-localEulerAngles.html
---
--- @param angles Vec3 The new local rotation
function M.ObjectTransform:set_local_euler_angles(angles)
  UTransform_SetLocalEulerAngles(unwrap(self), angles.x, angles.y, angles.z)
end

--- Get the local scale
---
--- See https://docs.unity3d.com/ScriptReference/Transform-localScale.html
---
--- @return Vec3
function M.ObjectTransform:get_local_scale()
  local x, y, z = UTransform_GetLocalScale(unwrap(self))
  return mathx.Vec3.new(x, y, z)
end

--- Set the local scale
---
--- See https://docs.unity3d.com/ScriptReference/Transform-localScale.html
---
--- @param scale Vec3 The new local scale
function M.ObjectTransform:set_local_scale(scale)
  UTransform_SetLocalScale(unwrap(self), scale.x, scale.y, scale.z)
end

--- Rotate the transform to look at the given position
---
--- See https://docs.unity3d.com/ScriptReference/Transform.LookAt.html
---
--- **Example: Making a camera and pointing it to 0, 4, 0**
---
--- .. code-block:: lua
---
---    local camera, camera_go = unity.Camera.spawn()
---    local camera_tf = camera_go:get_transform()
---
---    camera_tf:set_local_position(Vec3.new(20, 30, 0))
---    camera_tf:look_at(Vec3.new(0, 4, 0))
---
--- @param position Vec3 Position to look at
--- @param up Vec3|nil If nil, will use ``(0, 1, 0)``
function M.ObjectTransform:look_at(position, up)
  up = up or Vec3.new(0, 1, 0)
  UTransform_LookAt(unwrap(self), position.x, position.y, position.z, up.x, up.y, up.z)
end

return M
