local event = require("trombx.event")
local imgui = require("trombx.imgui")
local test = require("trombx.test")

local TrombX = CLRPackage("TrombX", "TrombX")

luanet.load_assembly("UnityEngine.CoreModule")
luanet.load_assembly("UnityEngine.AudioModule")
local UnityEngine = luanet.namespace("UnityEngine")

local UCamera = UnityEngine.Camera
local UCameraClearFlags = UnityEngine.CameraClearFlags
local UCameraClearFlags_Skybox = luanet.enum(UCameraClearFlags, "Skybox")
local UCameraClearFlags_SolidColor = luanet.enum(UCameraClearFlags, "SolidColor")
local UCameraClearFlags_Depth = luanet.enum(UCameraClearFlags, "Depth")
local UCameraClearFlags_Nothing = luanet.enum(UCameraClearFlags, "Nothing")

local get_breath_used_impl = TrombX.ScriptEngineBridge.GetBreathUsed
local set_breath_used_impl = TrombX.ScriptEngineBridge.SetBreathUsed
local UAudioSource_GetTime = TrombX.ScriptEngineBridge.AudioSource_GetTime
local UAudioSource_SetTime = TrombX.ScriptEngineBridge.AudioSource_SetTime

--- @class trombx.game
local M = {}

-- TODO: Use game_controller.musictrack instead?
-- And assert that they exist
local music_loader = UnityEngine.GameObject.Find("MUSIC_LOADER")
local music_source = music_loader:GetComponent(luanet.ctype(UnityEngine.AudioSource))
local music_length = music_source.clip.length

local tempo = TrombX.ScriptEngineBridge.GetCurrentModTrackData().tempo
local secs_per_beat = 60 / tempo

--- Returns the tempo of the current song, in BPM
---
--- Right now, Trombone Champ only supports one tempo value for a song, with no BPM changes
---
--- @return number
function M.get_tempo()
  return tempo
end

-- Updated in update_pre
-- We cache current time within a frame since FFI can be slow and this function can be hot
local current_time_cached = 0

--- Returns the number of seconds that have elapsed since the start of the song
---
--- Does not account for any latency offsets the player may have set
---
--- @return number
function M.get_current_time()
  return current_time_cached
end

--- Set the current music position, in seconds
---
--- @param time number The time to set, in seconds
function M.set_current_time(time)
  UAudioSource_SetTime(music_source, time)
  current_time_cached = time
end

--- Returns the number of beats that have elapsed since the start of the song
---
--- Does not account for any latency offsets the player may have set
---
--- @return number
function M.get_current_beat()
  return current_time_cached / secs_per_beat
end

--- Returns how much breath the player has used
---
--- Increases while tooting, decreases while not tooting
---
--- @return number
function M.get_breath_used()
  return get_breath_used_impl()
end

--- Set the amount of breath the player has used up
---
--- You can set this to 0 every frame to have infinite breath. For example:
---
--- .. code-block:: lua
---
---    local event = require("trombx.event")
---    local game = require("trombx.game")
---
---    event.update_post:bind(function()
---      game.set_breath_used(0)
---    end)
---
--- @param used number The amount of breath to have used up
function M.set_breath_used(used)
  set_breath_used_impl(used)
end

event.update_pre:bind(function()
  current_time_cached = UAudioSource_GetTime(music_source)
end)

event.update_post:bind(function()
  if imgui.begin_win("Audio") then
    -- Play/pause
    if imgui.button("||") then
      music_source:Pause()
    end
    imgui.same_line()
    if imgui.button(">") then
      music_source:UnPause()
    end

    -- Time displays
    local new_time, interacted = imgui.slider_float("Music time (seconds)", M.get_current_time(), 0, music_length)
    if interacted then
      M.set_current_time(new_time)
    end
    imgui.text("Music beat: %f (%.0f BPM)", M.get_current_beat(), M.get_tempo())
  end
  imgui.end_win()

  if imgui.begin_win("Render order") then
    for cam in luanet.each(UCamera.allCameras) do
      -- Show clearing if this camera does clearing
      local clear_flags = cam.clearFlags
      if clear_flags == UCameraClearFlags_Skybox then
        imgui.text_disabled("Clear with skybox")
      elseif clear_flags == UCameraClearFlags_SolidColor then
        imgui.text_disabled("Clear with solid color") -- TODO: Preview the color here?
      elseif clear_flags == UCameraClearFlags_Depth then
        imgui.text_disabled("Clear depth")
      elseif clear_flags == UCameraClearFlags_Nothing then
        -- Nothing
      else
        imgui.text_disabled("Unknown clear flags")
      end

      imgui.text("% 7.1f: %s", cam.depth, cam.name)

      -- Inspect with UnityExplorer
      imgui.same_line()
      if imgui.small_button("Inspect##" .. tostring(cam)) then
        local try_open_in_ue = function()
          luanet.load_assembly("UnityExplorer.BIE5.Mono")
          local UnityExplorer = luanet.namespace("UnityExplorer")
          local UnityExplorerUI = luanet.namespace("UnityExplorer.UI")
          UnityExplorer.InspectorManager.Inspect(cam)
          UnityExplorerUI.UIManager.ShowMenu = true
        end
        local ok, err = pcall(try_open_in_ue) -- TODO: Show warning in ImGui if this errors
        if not ok then
          print(err)
        end
      end

      local num_command_buffers = cam.commandBufferCount
      if num_command_buffers > 0 then
        imgui.text_disabled("Then %d command buffers", num_command_buffers)
      end

      -- TODO: When we setup post-pro support, show post pro passes here
    end
  end
  imgui.end_win()
end)

-- Check that functions return the expected type, and that we don't error out on any of these
test.define("trombx.game.no_error_and_expected_types", function()
  test.wait_next_frame() -- Give update_pre a chance to run
  test.assert_number(M.get_tempo())
  test.assert_number(M.get_current_time())
  M.set_current_time(24)
  test.assert_number(M.get_current_beat())
end)

test.define("trombx.game.set_time_changes_get_time_value", function()
  M.set_current_time(5.4)
  test.assert_nearly_eq(M.get_current_time(), 5.4)
  test.assert_nearly_eq(M.get_current_beat(), 5.4 / secs_per_beat)
end)

return M
