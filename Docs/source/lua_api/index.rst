Lua API
=======

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   trombx.container
   trombx.mathx
   trombx.event
   trombx.game
   trombx.note
   trombx.notefield
   trombx.notefield.mod
   trombx.notefield.mod.core
   trombx.notefield.mod.zigzag
   trombx.notefield.mod.decelerate
   trombx.scene_preset
   trombx.unity
   trombx.unity.unity_object
   trombx.unity.game_object
   trombx.unity.component
   trombx.unity.behaviour
   trombx.unity.object_transform
   trombx.unity.camera
   trombx.unity.asset_bundle
   trombx.imgui
   trombx.profiler
   trombx.test
