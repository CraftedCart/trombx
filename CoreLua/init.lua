local UnityEngine = CLRPackage("UnityEngine.CoreModule", "UnityEngine")
local BepInEx = CLRPackage("BepInEx", "BepInEx")
local SystemIO = CLRPackage("System.Runtime", "System.IO")
local TrombX = CLRPackage("TrombX", "TrombX")

-- Override `print` to go to Unity's `Debug.Log`
function print(...)
  local args = {...}
  for k, v in ipairs(args) do
    args[k] = tostring(v)
  end

  UnityEngine.Debug.Log("[TrombX Lua] " .. table.concat(args, "\t"))
end

-- Add a directory to package.path
local function add_module_root(path)
  local lua_files = SystemIO.Path.Combine(path, "?.lua")
  local lua_init_files = SystemIO.Path.Combine(path, "?", "init.lua")
  package.path = lua_files .. ";" .. lua_init_files .. ";" .. package.path
end

-- Clear out paths, we'll add our own later
package.path = ""
package.cpath = ""

local trombx_module_root = SystemIO.Path.Combine(BepInEx.Paths.PluginPath, "TrombX", "lua")
add_module_root(trombx_module_root)

-- TrombX package path has been added - we can require stuff now
local sandbox = require("trombx_private.sandbox")
local debug_ui = require("trombx_private.debug_ui")
require("trombx.profiler") -- Requiring this sets up its ImGui UI

debug_ui.init()

print("Init script loaded")

print("Loading track script")

-- Add the track's `lua` dir to package.path
local current_track_dir = TrombX.ScriptEngineBridge.GetCurrentModTrackDirectory()
local track_module_root = SystemIO.Path.Combine(current_track_dir, "lua")
-- add_module_root(track_module_root) -- (No longer needed now that we load modules ourself in the sandbox env)

-- Load the track's init script
local track_init_path = SystemIO.Path.Combine(current_track_dir, "init.lua")
local func, err = loadfile(track_init_path, "t")
if func ~= nil then
  local sandbox_env = sandbox.make_env{
    require = {
      paths = {
        { path = SystemIO.Path.Combine(track_module_root, "?.lua"), priviledge = "untrusted" },
        { path = SystemIO.Path.Combine(track_module_root, "?", "init.lua"), priviledge = "untrusted" },
        { path = SystemIO.Path.Combine(trombx_module_root, "?.lua"), priviledge = "trusted" },
        { path = SystemIO.Path.Combine(trombx_module_root, "?", "init.lua"), priviledge = "trusted" },
      },

      -- Patterns to pass through to the regular `require()` function
      -- Contains LuaJIT extensions here
      -- NOTE: If updating this list, also update the sandbox docs
      passthrough_patterns = {
        "^bit$",
        "^table%.new$",
        "^table%.clear$",
      },

      -- Patterns to raise an error on
      denied_patterns = {
        "^trombx_private%.", -- Don't allow charts to access internals
      },
    },
  }
  setfenv(func, sandbox_env)
  func()
else
  print("Failed to load track script: " .. err)
end

print("Loaded track script")
