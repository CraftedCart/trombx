local mathx = require("trombx.mathx")

local Vec3 = mathx.Vec3

--- @class trombx.notefield.mod.core
local M = {}

--- TODO: Should this not have Note in the name since it applies to the receptor line/receptor too?
---
--- @class NoteModContext
--- @field public notefield NotefieldX The notefield
--- @field public orig_position Vec3 Position of a point within the note or receptor line. X approaches 0 as a note gets closer to the receptors, Y is the apparent pitch of the note, Z is forwards/backwards. Modifiers should not modify this, but rather should modify ``position`` to move points of a note around.
--- @field public position Vec3 Contains a copy of ``orig_position`` by default. Modifiers should modify this to change the position of point in a note.
--- @field public beats_until_receptor number How many beats away the current ``position`` is until it reaches the receptor. Goes negative when past the receptor. Will always be 0 when modifying the receptor line.
M.NoteModContext = {}
local NoteModContext_metatable = { __index = M.NoteModContext }

-- NoteModContexts are intended to be created from the notefield, and don't have a public new function here
function M.NoteModContext._new()
  return setmetatable(
    {
      notefield = nil,
      orig_position = Vec3.new(),
      position = Vec3.new(),
      beats_until_receptor = nil,
    },
    NoteModContext_metatable
    )
end

--- Base class for note modifiers
---
--- @class NoteModBase
M.NoteModBase = {}
local NoteModBase_metatable = { __index = M.NoteModBase }

--- Called several times per displayed note to modify a particular position in a note
---
--- This base class has does nothing in its implementation - modifiers should override this and put their own
--- implementation here
---
--- @param ctx NoteModContext The note to modify and additional info that may be handy.
function M.NoteModBase:modify_note(ctx)
  -- Nothing - inheritors should override this
end

--- Can this mod provide a debug UI with ImGui?
---
--- The default implementation returns false
---
--- @return boolean
function M.NoteModBase:is_imgui_inspectable()
  return false
end

--- Provide a debug UI with ImGui
---
--- The default implementation does nothing
function M.NoteModBase:imgui_inspect()
  -- Nothing - inheritors may override this
end

return M
