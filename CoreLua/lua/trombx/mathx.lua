local test = require("trombx.test")

local abs = math.abs
local min = math.min
local max = math.max
local floor = math.floor
local ceil = math.ceil
local sqrt = math.sqrt

--- @class trombx.mathx
local M = {}

--- Round to the nearest integer
---
--- For ?.5 values, we always round towards positive infinity
---
--- @param val number The value to round
---
--- @return integer
function M.round(val)
  return floor(val + 0.5)
end

--- Round toward zero
---
--- @param val number The value to round
---
--- @return integer
function M.truncate(val)
  return val >= 0 and floor(val) or ceil(val)
end

--- Linearly interpolate a value between ``a`` and ``b``
---
--- Does not clamp if ``alpha`` is outside of the 0 - 1 range
---
--- @param a number The start point
--- @param b number The end point
--- @param alpha number 0 = ``a``, 1 = ``b``, numbers in between are interpolated between ``a`` and ``b``
---
--- @return number
function M.lerp(a, b, alpha)
  return ((b - a) * alpha) + a
end

--- Remap a value from one range to another
---
--- @param value number The number to remap
--- @param start1 number The start point for the original range
--- @param end1 number The end point for the original range
--- @param start2 number The start point for the new range
--- @param end2 number The end point for the new range
---
--- @return number
function M.remap(value, start1, end1, start2, end2)
  return (value - start1) / (end1 - start1) * (end2 - start2) + start2
end

--- Clamp a number between a min and max
---
--- @param value number The number to clamp
--- @param min_value number The minimum allowed value
--- @param max_value number The maximum allowed value
---
--- @return number
function M.clamp(value, min_value, max_value)
  return min(max(value, min_value), max_value)
end

--- Returns whether the given number is NaN
---
--- @param val number The number to check
---
--- @return boolean
function M.is_nan(val)
  -- IEEE 754 states that a NaN value is not equal to itself
  return val ~= val
end

--- Returns if two numbers are nearly equal to each other, within a certain tolerance
---
--- @param a number The first number
--- @param b number The second number
--- @param tolerance number|nil How close they have to be to each other to return true - defaults to 0.000001 if ``nil``
---
--- @return boolean
function M.nearly_equal(a, b, tolerance)
  tolerance = tolerance or 0.000001
  return abs(a - b) < tolerance
end

--- Three-dimensional vector
---
--- Implements the following metamethods:
---
--- - ``__eq(other: Vec3) -> boolean``: Check for equality with another vector
--- - ``__add(other: Vec3) -> Vec3``: Component-wide addition
--- - ``__sub(other: Vec3) -> Vec3``: Component-wide subtraction
--- - ``__mul(other: Vec3) -> Vec3``: Component-wide multiplication
--- - ``__div(other: Vec3) -> Vec3``: Component-wide division
--- - ``__unm() -> Vec3``: Negates all components
--- - ``__tostring() -> string``: Makes a human-readable string of the vector's values
---
--- @class Vec3
--- @field public x number X value
--- @field public y number Y value
--- @field public z number Z value
--- @operator add(Vec3): Vec3
--- @operator sub(Vec3): Vec3
--- @operator mul(Vec3): Vec3
--- @operator div(Vec3): Vec3
--- @operator unm: Vec3
M.Vec3 = {}
local Vec3_metatable = {
  __index = M.Vec3,
  __eq = function(self, other) return self.x == other.x and self.y == other.y and self.z == other.z end,
  __add = function(self, other) return M.Vec3.new(self.x + other.x, self.y + other.y, self.z + other.z) end,
  __sub = function(self, other) return M.Vec3.new(self.x - other.x, self.y - other.y, self.z - other.z) end,
  __mul = function(self, other) return M.Vec3.new(self.x * other.x, self.y * other.y, self.z * other.z) end,
  __div = function(self, other) return M.Vec3.new(self.x / other.x, self.y / other.y, self.z / other.z) end,
  __unm = function(self) return M.Vec3.new(-self.x, -self.y, -self.z) end,
  __tostring = function(self) return string.format("<Vec3 %f, %f, %f>", self.x, self.y, self.z) end,
}

--- Create a new :lua:class:`Vec3` from X/Y/Z components
---
--- @param x number|nil X value (or 0 if not given)
--- @param y number|nil Y value (or 0 if not given)
--- @param z number|nil Z value (or 0 if not given)
---
--- @return Vec3
function M.Vec3.new(x, y, z)
  return setmetatable(
    {
      x = x or 0,
      y = y or 0,
      z = z or 0,
    },
    Vec3_metatable
    )
end

--- Create a new :lua:class:`Vec3` with identical X/Y/Z components
---
--- @param value number The value for the X, Y, and Z components
---
--- @return Vec3
function M.Vec3.splat(value)
  return setmetatable(
    {
      x = value,
      y = value,
      z = value,
    },
    Vec3_metatable
    )
end

--- Get the length/magnitude of the vector
---
--- @return number
function M.Vec3:length()
  return sqrt(self.x ^ 2 + self.y ^ 2 + self.z ^ 2)
end

--- Get the length/magnitude of the vector, squared
---
--- Faster than :lua:meth:`Vec3.length` since it doesn't have to square root
---
--- @return number
function M.Vec3:length_squared()
  return self.x ^ 2 + self.y ^ 2 + self.z ^ 2
end

--- Convert to a :lua:class:`Vec2`, discarding the Z component
---
--- @return Vec2
function M.Vec3:to_vec2()
  return M.Vec2.new(self.x, self.y)
end

--- Two-dimensional vector
---
--- Implements the following metamethods:
---
--- - ``__eq(other: Vec2) -> boolean``: Check for equality with another vector
--- - ``__add(other: Vec2) -> Vec2``: Component-wide addition
--- - ``__sub(other: Vec2) -> Vec2``: Component-wide subtraction
--- - ``__mul(other: Vec2) -> Vec2``: Component-wide multiplication
--- - ``__div(other: Vec2) -> Vec2``: Component-wide division
--- - ``__unm() -> Vec2``: Negates all components
--- - ``__tostring() -> string``: Makes a human-readable string of the vector's values
---
--- @class Vec2
--- @field public x number X value
--- @field public y number Y value
--- @operator add(Vec2): Vec2
--- @operator sub(Vec2): Vec2
--- @operator mul(Vec2): Vec2
--- @operator div(Vec2): Vec2
--- @operator unm: Vec2
M.Vec2 = {}
local Vec2_metatable = {
  __index = M.Vec2,
  __eq = function(self, other) return self.x == other.x and self.y == other.y end,
  __add = function(self, other) return M.Vec2.new(self.x + other.x, self.y + other.y) end,
  __sub = function(self, other) return M.Vec2.new(self.x - other.x, self.y - other.y) end,
  __mul = function(self, other) return M.Vec2.new(self.x * other.x, self.y * other.y) end,
  __div = function(self, other) return M.Vec2.new(self.x / other.x, self.y / other.y) end,
  __unm = function(self) return M.Vec2.new(-self.x, -self.y) end,
  __tostring = function(self) return string.format("<Vec2 %f, %f>", self.x, self.y) end,
}

--- Create a new :lua:class:`Vec2` from X/Y components
---
--- @param x number|nil X value (or 0 if not given)
--- @param y number|nil Y value (or 0 if not given)
---
--- @return Vec2
function M.Vec2.new(x, y)
  return setmetatable(
    {
      x = x or 0,
      y = y or 0,
    },
    Vec2_metatable
    )
end

--- Create a new :lua:class:`Vec2` with identical X/Y components
---
--- @param value number The value for the X and Y components
---
--- @return Vec2
function M.Vec2.splat(value)
  return setmetatable(
    {
      x = value,
      y = value,
    },
    Vec2_metatable
    )
end

--- Get the length/magnitude of the vector
---
--- @return number
function M.Vec2:length()
  return sqrt(self.x ^ 2 + self.y ^ 2)
end

--- Get the length/magnitude of the vector, squared
---
--- Faster than :lua:meth:`Vec2.length` since it doesn't have to square root
---
--- @return number
function M.Vec2:length_squared()
  return self.x ^ 2 + self.y ^ 2
end

--- Rectangle
---
--- Implements the following metamethods:
---
--- - ``__eq(other: Rect) -> boolean``: Check for equality with another rect
--- - ``__tostring() -> string``: Makes a human-readable string of the rect's values
---
--- @class Rect
--- @field public position Vec2
--- @field public size Vec2
M.Rect = {}
local Rect_metatable = {
  __index = M.Rect,
  __eq = function(self, other)
    return self.position == other.position and self.size == other.size
  end,
  __tostring = function(self)
    return string.format("<Rect %f, %f, %f, %f>", self.pos.x, self.pos.y, self.size.x, self.size.y)
  end,
}

--- Create a new :lua:class:`Rect` from a position and a size
---
--- @param position Vec2
--- @param size Vec2
---
--- @return Rect
function M.Rect.new(position, size)
  return setmetatable(
    {
      position = position,
      size = size,
    },
    Rect_metatable
    )
end

--- Create a new :lua:class:`Rect` X/Y position and W/H height numbers
---
--- @param x number X position
--- @param y number Y position
--- @param w number Width
--- @param h number Height
---
--- @return Rect
function M.Rect.from_xywh(x, y, w, h)
  return setmetatable(
    {
      position = M.Vec2.new(x, y),
      size = M.Vec2.new(w, h),
    },
    Rect_metatable
    )
end

--- Rgba
---
--- Implements the following metamethods:
---
--- - ``__eq(other: Rgba) -> boolean``: Check for equality with another Rgba
--- - ``__tostring() -> string``: Makes a human-readable string of the color's values
---
--- @class Rgba
--- @field r number
--- @field g number
--- @field b number
--- @field a number
M.Rgba = {}
local Rgba_metatable = {
  __index = M.Rgba,
  __eq = function(self, other)
    return self.r == other.r and self.g == other.g and self.b == other.b and self.a == other.a
  end,
  __tostring = function(self)
    return string.format("<Rgba %f, %f, %f, %f>", self.r, self.g, self.b, self.a)
  end,
}

--- Create a new :lua:class:`Color` from RGBA values
---
--- Values should be on a scale form 0 - 1
---
--- @param r number Red
--- @param g number Green
--- @param b number Blue
--- @param a number Alpha
---
--- @return Rgba
function M.Rgba.new(r, g, b, a)
  return setmetatable(
    {
      r = r,
      g = g,
      b = b,
      a = a,
    },
    Rgba_metatable
    )
end

test.define("trombx.mathx.round", function()
  test.assert_eq(M.round(5), 5)
  test.assert_eq(M.round(5.2), 5)
  test.assert_eq(M.round(5.5), 6)
  test.assert_eq(M.round(5.7), 6)
  test.assert_eq(M.round(-5.7), -6)
  test.assert_eq(M.round(-5.5), -5)
  test.assert_eq(M.round(-5), -5)
end)

test.define("trombx.mathx.truncate", function()
  test.assert_eq(M.truncate(5), 5)
  test.assert_eq(M.truncate(5.2), 5)
  test.assert_eq(M.truncate(5.5), 5)
  test.assert_eq(M.truncate(5.7), 5)
  test.assert_eq(M.truncate(-5.7), -5)
  test.assert_eq(M.truncate(-5.5), -5)
  test.assert_eq(M.truncate(-5), -5)
end)

test.define("trombx.mathx.clamp", function()
  test.assert_eq(M.clamp(5, 2, 7), 5)
  test.assert_eq(M.clamp(-12, 2, 7), 2)
  test.assert_eq(M.clamp(48, 2, 7), 7)
end)

test.define("trombx.mathx.is_nan", function()
  assert(M.is_nan(0 / 0))
  assert(not M.is_nan(10))
end)

test.define("trombx.mathx.Vec3.length", function()
  test.assert_nearly_eq(M.Vec3.new(5, 2, 1):length(), 5.47722557505)
end)

test.define("trombx.mathx.Vec3.length_squared", function()
  test.assert_nearly_eq(M.Vec3.new(5, 2, 1):length_squared(), 30)
end)

return M
