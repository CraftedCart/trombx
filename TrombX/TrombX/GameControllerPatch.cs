// ReSharper disable InconsistentNaming

using BaboonAPI.Hooks.Tracks;
using HarmonyLib;
using UnityEngine;

namespace TrombX
{
    public class GameControllerPatch
    {
        private static bool isPlayingModTrack;

        [HarmonyPatch(typeof(GameController), nameof(GameController.Start))]
        [HarmonyPostfix]
        static void Start_Post(GameController __instance)
        {
            if (__instance.freeplay)
            {
                isPlayingModTrack = false;
                TrombXPlugin.Instance.Logger.LogDebug("In freeplay, not loading TrombX scripts");
                return;
            }

            var currentTrack = TrackLookup.lookup(GlobalVariables.chosen_track_data.trackref);
            isPlayingModTrack = currentTrack is ModTrack;
            if (!isPlayingModTrack)
            {
                TrombXPlugin.Instance.Logger.LogDebug($"Not playing a modtrack (Current track: {currentTrack.trackref} - {currentTrack.GetType().FullName}), not loading TrombX scripts");
                return;
            }

            TrombXPlugin.Instance.Logger.LogInfo("Setting up TrombX for this track");

            // Setup debug UI
            var debugUiGo = new GameObject("TrombX Debug UI");

            var debugUiCamera = debugUiGo.AddComponent<Camera>();
            debugUiCamera.clearFlags = CameraClearFlags.Nothing;
            debugUiCamera.cullingMask = 0; // Don't draw the world at all - this camera only exists for custom drawing
            debugUiCamera.depth = 100f;

            debugUiGo.AddComponent<DebugUiRenderer>();

            ScriptEngineBridge.GameController = __instance;
            ScriptEngineBridge.TrackScriptEngine!.LoadTrackScripts(); // TODO: Does this error in free improv?
        }

        [HarmonyPatch(typeof(GameController), nameof(GameController.Update))]
        [HarmonyPrefix]
        static void Update_Pre()
        {
            if (!isPlayingModTrack)
            {
                return;
            }

            ScriptEngineBridge.EventUpdatePre.Invoke();
        }

        [HarmonyPatch(typeof(GameController), nameof(GameController.Update))]
        [HarmonyPostfix]
        static void Update_Post()
        {
            if (!isPlayingModTrack)
            {
                return;
            }

            ScriptEngineBridge.EventUpdatePost.Invoke();
        }
    }
}
