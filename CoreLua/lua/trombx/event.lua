local TrombX = CLRPackage("TrombX", "TrombX")

--- @class trombx.event
local M = {}

--- An event, generated from TrombX internal code, that modcharts may bind to
---
--- @class ClrEvent
M.ClrEvent = {}
local ClrEvent_metatable = { __index = M.ClrEvent }

-- Maps `ClrEvent` instances to their private members
-- We store this here instead of in the `ClrEvent` class directly to prevent making C#/CLR objects accessible at all to
-- track scripts - track code could potentially use this to break the sandbox
local ClrEvent_internals = setmetatable({}, { __mode = "k" })

-- Private and not exported
local function new_clr_event(clr_script_event)
  local out = setmetatable({}, ClrEvent_metatable)
  ClrEvent_internals[out] = { _script_event = clr_script_event }
  return out
end

--- Subscribe to an event
---
--- ``func`` will be called every time every time the event is emitted
---
--- @param func fun() The function to bind
---
--- @return fun() # The function passed in
function M.ClrEvent:bind(func)
  ClrEvent_internals[self]._script_event:Bind(func)
  return func
end

--- Unsubscribe from an event
---
--- If ``func`` was subscribed to the event, it will unsubscribed and will no longer be called when the event is
--- emitted. If ``func`` wasn't subscribed, nothing happens.
---
--- @param func fun() The function to unbind
function M.ClrEvent:unbind(func)
  ClrEvent_internals[self]._script_event:Unbind(func)
end

--- Emitted every frame at the beginning of ``GameController.Update``
---
--- Use this to run code before the game has a chance to process inputs and update the game state
---
--- @type ClrEvent
M.update_pre = new_clr_event(TrombX.ScriptEngineBridge.EventUpdatePre)

--- Emitted every frame at the end of ``GameController.Update``
---
--- Use this to run code after the game has had a chance to process inputs and update the game state
---
--- @type ClrEvent
M.update_post = new_clr_event(TrombX.ScriptEngineBridge.EventUpdatePost)

local function first_update()
  -- Register tests on first update to prevent cyclic require-ing
  local test = require("trombx.test")

  -- Test that both `update_pre` and `update_post` are called, in that order, only once within a frame
  test.define("trombx.event.update_events", function()
    local events_called = {}

    local update_pre_func = M.update_pre:bind(function() table.insert(events_called, "update_pre") end)
    local update_post_func = M.update_post:bind(function() table.insert(events_called, "update_post") end)
    test.defer(function()
      M.update_pre:unbind(update_pre_func)
      M.update_post:unbind(update_post_func)
    end)

    test.wait_next_frame()

    test.assert_eq(#events_called, 2)
    test.assert_eq(events_called[1], "update_pre")
    test.assert_eq(events_called[2], "update_post")
  end
  )

  M.update_pre:unbind(first_update)
end
M.update_pre:bind(first_update)

return M
