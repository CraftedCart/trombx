TrombX
======

A modcharting framework for Trombone Champ

Still heavily WIP for now - not particularly usable

Note: If building on Linux, you may need to use Mono's msbuild over the dotnet msbuild - just so NLua and KeraLua forks
here can be built with .NET framework 4.6. The NLua/KeraLua forks here are by no means polished and have just the
minimum required done to get them building with LuaJIT for TrombX. There are no-doubt several build configurations for
those forks that likely won't work.
