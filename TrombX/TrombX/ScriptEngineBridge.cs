using System.Collections.Generic;
using BaboonAPI.Hooks.Tracks;
using NLua;
using TrombLoader.CustomTracks;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;
using UnityEngine.Rendering;

namespace TrombX
{
    /// <summary>
    /// Basically just a variable/function dump that TrombX scripts can use
    ///
    /// Intended only to be used by TrombX's core Lua modules, not intended for general use by other plugins or what not
    /// </summary>
    public static class ScriptEngineBridge
    {
        /// <summary>
        /// The script engine for the currently playing track
        /// </summary>
        internal static ScriptEngine? TrackScriptEngine;

        /// <summary>
        /// The <c>GameController</c> for the currently playing track
        /// </summary>
        public static GameController? GameController;

        public static AssetBundle? TrombXAssets;

        // Exposed to scripts via trombx.event
        public static readonly ScriptEvent EventUpdatePre = new();
        public static readonly ScriptEvent EventUpdatePost = new();
        public static readonly ScriptEvent EventImGuiRender = new();

        internal static void ClearEvents()
        {
            EventUpdatePre.Clear();
            EventUpdatePost.Clear();
            EventImGuiRender.Clear();
        }

        public static string? GetCurrentModTrackDirectory() =>
            (TrackLookup.lookup(GlobalVariables.chosen_track_data.trackref) as ModTrack)?.Directory;

        public static CustomTrackData? GetCurrentModTrackData() =>
            (TrackLookup.lookup(GlobalVariables.chosen_track_data.trackref) as ModTrack)?.Data;

        /// <summary>
        /// Unity objects have special handling with null checks - we can't just compare against `nil` in Lua
        /// </summary>
        public static bool IsObjectValid(object? obj)
        {
            // I'm not entirely sure if this cast is necessary buuut let's roll with it for now
            if (obj is UnityEngine.Object unityObj)
            {
                return unityObj != null;
            }
            else
            {
                return obj != null;
            }
        }

        /// <summary>
        /// Calling this instead of the `UnityEngine.Vector3` constructor from Lua seems to be faster
        /// </summary>
        public static Vector3 MakeVector3(float x, float y, float z)
        {
            return new Vector3(x, y, z);
        }

        // Don't create a new list every time LineRendererSetPositions is called - just keep one and re-use it here
        // Especially useful when TootTally disables the GC during gameplay, so this helps to try and reduce ballooning
        // memory allocations somewhat
        private static readonly List<Vector3> PositionsList = new();

        /// <summary>
        /// A reasonably fast way to set the positions on a LineRenderer from Lua
        /// </summary>
        /// <param name="lineRenderer">The line renderer to set positions of</param>
        /// <param name="positions">A table of numbers, where each 3 numbers corresponds to X/Y/Z coordinates</param>
        public static void LineRendererSetPositions(LineRenderer lineRenderer, LuaTable positions)
        {
            var lua = TrackScriptEngine!.Lua;
            var oldTop = lua.State.GetTop();

            try
            {
                PositionsList.Clear();

                lua.Push(positions);
                for (var i = 1;; i += 3)
                {
                    lua.State.RawGetInteger(-1, i);
                    if (lua.State.IsNil(-1)) break;
                    var x = (float)lua.State.ToNumber(-1);
                    lua.State.Pop(1);

                    lua.State.RawGetInteger(-1, i + 1);
                    if (lua.State.IsNil(-1)) break;
                    var y = (float)lua.State.ToNumber(-1);
                    lua.State.Pop(1);

                    lua.State.RawGetInteger(-1, i + 2);
                    if (lua.State.IsNil(-1)) break;
                    var z = (float)lua.State.ToNumber(-1);
                    lua.State.Pop(1);

                    PositionsList.Add(new Vector3(x, y, z));
                }

                lineRenderer.SetPositions(PositionsList.ToArray());
            }
            finally
            {
                lua.State.SetTop(oldTop);
            }
        }

        public static void SetBreathUsed(float used) => GameController!.breathcounter = used;

        public static float GetBreathUsed() => GameController!.breathcounter;


        /// <summary>
        /// Set vertex buffer data from what Dear ImGui spits out
        ///
        /// 53 bits of precision (from a Lua double) should be enough for a pointer
        /// </summary>
        /// <param name="mesh">Mesh</param>
        /// <param name="ptr">ImDrawVert pointer</param>
        /// <param name="length">Number of ImDrawVerts</param>
        public static void ImGuiSetMeshVertexBuffer(Mesh mesh, ulong ptr, int length)
        {
            unsafe
            {
                var vertArray = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<DebugUiRenderer.ImDrawVert>(
                    (void*)ptr,
                    length,
                    Allocator.None
                );

                mesh.SetVertexBufferData(vertArray, 0, 0, length);
            }
        }

        /// <summary>
        /// Set index buffer data from what Dear ImGui spits out
        ///
        /// 53 bits of precision (from a Lua double) should be enough for a pointer
        /// </summary>
        /// <param name="mesh">Mesh</param>
        /// <param name="ptr">ImDrawIdx pointer</param>
        /// <param name="length">Number of ImDrawIdxs</param>
        public static void ImGuiSetMeshIndexBuffer(Mesh mesh, ulong ptr, int length)
        {
            unsafe
            {
                var indexArray = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<ushort>(
                    (void*)ptr,
                    length,
                    Allocator.None
                );

                mesh.SetIndexBufferData(indexArray, 0, 0, length);
            }
        }

        private static readonly CommandBuffer ImGuiCommandBuffer = new();
        public static void ImGuiDraw(
            Mesh mesh,
            Material material,
            float clipRectX,
            float clipRectY,
            float clipRectZ,
            float clipRectW,
            int idxOffset,
            int elemCount
        )
        {
            ImGuiCommandBuffer.Clear();
            ImGuiCommandBuffer.SetProjectionMatrix(Matrix4x4.Ortho(
                0,
                Screen.width,
                Screen.height,
                0,
                -1000,
                1000
            ));
            ImGuiCommandBuffer.EnableScissorRect(new Rect(
                clipRectX,
                Screen.height - clipRectW,
                clipRectZ - clipRectX,
                clipRectW - clipRectY
            ));
            ImGuiCommandBuffer.DrawMesh(mesh, Matrix4x4.identity, material);

            mesh.SetSubMesh(
                0,
                new SubMeshDescriptor(idxOffset, elemCount),
                MeshUpdateFlags.DontValidateIndices | MeshUpdateFlags.DontNotifyMeshUsers | MeshUpdateFlags.DontRecalculateBounds
            );

            Graphics.ExecuteCommandBuffer(ImGuiCommandBuffer);
        }

        public static Texture2D TextureFromRgba32(ulong ptr, int width, int height)
        {
            unsafe
            {
                var bytes = width * height * 4;
                var pixelArray = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<byte>(
                    (void*)ptr,
                    bytes,
                    Allocator.None
                );

                var tex = new Texture2D(width, height, TextureFormat.RGBA32, false);
                tex.SetPixelData(pixelArray, 0);
                tex.Apply(updateMipmaps: false);
                return tex;
            }
        }

        public static void GameObject_SetActive(GameObject go, bool active)
        {
            go.SetActive(active);
        }

        public static bool GameObject_IsActiveSelf(GameObject go)
        {
            return go.activeSelf;
        }

        public static bool GameObject_IsActiveInHierarchy(GameObject go)
        {
            return go.activeInHierarchy;
        }

        public static Transform GameObject_GetTransform(GameObject go)
        {
            return go.transform;
        }

        public static void Transform_GetLocalPosition(Transform tf, out float x, out float y, out float z)
        {
            var localPosition = tf.localPosition;
            x = localPosition.x;
            y = localPosition.y;
            z = localPosition.z;
        }

        public static void Transform_SetLocalPosition(Transform tf, float x, float y, float z)
        {
            tf.localPosition = new Vector3(x, y, z);
        }

        public static void Transform_GetLocalEulerAngles(Transform tf, out float x, out float y, out float z)
        {
            var localEulerAngles = tf.localEulerAngles;
            x = localEulerAngles.x;
            y = localEulerAngles.y;
            z = localEulerAngles.z;
        }

        public static void Transform_SetLocalEulerAngles(Transform tf, float x, float y, float z)
        {
            tf.localEulerAngles = new Vector3(x, y, z);
        }

        public static void Transform_GetLocalScale(Transform tf, out float x, out float y, out float z)
        {
            var localScale = tf.localScale;
            x = localScale.x;
            y = localScale.y;
            z = localScale.z;
        }

        public static void Transform_SetLocalScale(Transform tf, float x, float y, float z)
        {
            tf.localScale = new Vector3(x, y, z);
        }

        public static void Transform_LookAt(Transform tf, float x, float y, float z, float upX, float upY, float upZ)
        {
            tf.LookAt(new Vector3(x, y, z), new Vector3(upX, upY, upZ));
        }

        public static float AudioSource_GetTime(AudioSource audioSource)
        {
            return audioSource.time;
        }

        public static void AudioSource_SetTime(AudioSource audioSource, float time)
        {
            audioSource.time = time;
        }
    }
}
