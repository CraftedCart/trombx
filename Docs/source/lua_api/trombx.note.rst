trombx.note
===========

.. contents:: :local:

Description
-----------

Note and note data types

API reference
-------------

.. lua:automodule:: trombx.note
