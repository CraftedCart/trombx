--- @class trombx_private.interop.storage
local M = {}

--- @class UObject

--- @class UGameObject: UObject
---
--- @field activeSelf boolean
--- @field activeInHierarchy boolean
--- @field isStatic boolean
--- @field layer integer
--- @field scene UScene
--- @field sceneCullingMask integer
--- @field tag string
--- @field transform UTransform
--- @field name string
---
--- @field SetActive fun(UGameObject, boolean)
--- @field AddComponent fun(UGameObject, any)

--- @class UComponent: UObject
--- @field gameObject UGameObject

--- @class UBehaviour: UComponent
---
--- @field enabled boolean

--- @class UTransform: UComponent
---
--- @field SetParent fun(UTransform, boolean)

--- @class UCamera: UBehaviour
---
--- @field depth number
--- @field cullingMask number
--- @field clearFlags any
--- @field rect any
--- @field backgroundColor any

--- @class UAssetBundle: UObject
---
--- @field LoadAsset fun(UAssetBundle, string)

--- @class UScene

--- @type { [UnityObject]: UObject }
local lua_to_unity_proxies = setmetatable({}, { __mode = "k" })
--- @type { [UObject]: UnityObject }
local unity_to_lua_proxies = setmetatable({}, { __mode = "v" })

--- @param lua_object UnityObject
--- @return UObject
function M.unwrap(lua_object)
  return lua_to_unity_proxies[lua_object]
end

--- @param unity_object UObject
--- @return UnityObject|nil
function M.get_wrapped(unity_object)
  return unity_to_lua_proxies[unity_object]
end

--- @generic T
---
--- @param unity_object UObject
--- @param ty T # Type representing the Unity object in Lua
--- @return T
function M.get_or_make_wrapped(unity_object, ty)
  local wrapped = M.get_wrapped(unity_object)
  if wrapped == nil then
    wrapped = setmetatable({}, { __index = ty })
    M.store(wrapped, unity_object)
  end

  return wrapped
end

--- @param lua_object UnityObject
--- @param unity_object UObject
function M.store(lua_object, unity_object)
  lua_to_unity_proxies[lua_object] = unity_object
  unity_to_lua_proxies[unity_object] = lua_object
end

return M
