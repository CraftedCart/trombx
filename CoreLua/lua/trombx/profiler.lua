local event = require("trombx.event")
local imgui = require("trombx.imgui")
local profile = require("jit.profile")

--- @class trombx.profiler
local M = {}

local path_sep = jit.os == "Windows" and "\\" or "/"

--- @class FileLines
--- @field lines string[]

--- @type table<string, FileLines>
local file_contents = {}

--- Read a file's lines into ``file_contents``
---
--- If the file is not readable, we store an empty file into ``file_contents``
local function cache_file(path)
  local inner = function()
    local lines = {}
    for line in io.lines(path) do
      table.insert(lines, line)
    end
    file_contents[path] = { lines = lines }
  end

  if not pcall(inner) then
    -- Cache an empty file if it's not readable
    file_contents[path] = { lines = {} }
  end
end

--- Get the text at a given line in a file
---
--- @param path string Filepath
--- @param line_number integer Line to get
---
--- @return string|nil # The line, or ``nil`` if ``line_number`` is out-of-bounds or we failed to read the file
local function get_line(path, line_number)
  local file_lines = file_contents[path]
  if file_lines == nil then
    cache_file(path)
    file_lines = file_contents[path]
  end
  assert(file_lines ~= nil)

  return file_lines.lines[line_number]
end

--- @class ProfiledSamples
--- @field stack_frames StackFrame[]
--- @field samples integer

--- @class StackFrame
--- @field func_name string
--- @field func_name_short string
--- @field path_and_line string
--- @field filename_and_line string The last part of ``path_and_line``
--- @field path string
--- @field line integer
local StackFrame = {}
local StackFrame_metatable = { __index = StackFrame }

--- @param func_name string
--- @param path_and_line string
--- @return StackFrame
function StackFrame.new(func_name, path_and_line)
  -- In case func_name is a path, get the last part of the path
  local func_name_short
  for component in func_name:gmatch("([^" .. path_sep .. "]+)") do
    func_name_short = component
  end

  -- Get the last part of the path
  local filename_and_line
  for component in path_and_line:gmatch("([^" .. path_sep .. "]+)") do
    filename_and_line = component
  end

  -- Split the path and line number components
  local split_path_and_line = {}
  for part in path_and_line:gmatch("([^:]+)") do
    table.insert(split_path_and_line, part)
  end
  local line = tonumber(split_path_and_line[#split_path_and_line])
  split_path_and_line[#split_path_and_line] = nil
  local path = table.concat(split_path_and_line, ":")

  return setmetatable(
    {
      func_name = func_name,
      func_name_short = func_name_short,
      path_and_line = path_and_line,
      filename_and_line = filename_and_line,
      path = path,
      line = line,
    },
    StackFrame_metatable
    )
end

--- Key is a raw stack dump
---
--- @type table<string, ProfiledSamples>
local all_samples = {}

--- Sorted from high-to-low sample counts
---
--- @type ProfiledSamples[]
local samples_sorted = {}

--- Total number of samples
local num_samples = 0

--- @param thread thread
--- @param samples integer
--- @param vm_state string
local function profiler_callback(thread, samples, vm_state)
  num_samples = num_samples + samples

  -- We use \1 as a marker to split the string with. \0 doesn't work with gmatch.
  local stack_dump_raw = profile.dumpstack(thread, "pf\1l\1", 100)
  local reading_func_name = true
  local current_func_name
  local stack_frames = {} --- @type StackFrame[]

  -- Ping-pong between reading a function name and the function's path/line number
  for part in stack_dump_raw:gmatch("([^\1]+)") do
    if reading_func_name then
      current_func_name = part

      reading_func_name = false
    else
      table.insert(stack_frames, StackFrame.new(current_func_name, part))

      reading_func_name = true
    end
  end

  local profiled_samples = all_samples[stack_dump_raw]
  if profiled_samples ~= nil then
    -- We already sampled this place before, increment its sample count
    profiled_samples.samples = profiled_samples.samples + 1

    table.sort(samples_sorted, function(a, b)
      return a.samples > b.samples
    end)
  else
    -- This is a new place we're sampling

    --- @type ProfiledSamples
    profiled_samples = {
      stack_frames = stack_frames,
      samples = 1,
    }

    all_samples[stack_dump_raw] = profiled_samples
    table.insert(samples_sorted, profiled_samples)
  end
end

--- Start profiling
---
--- Results can be seen in the profiler window on the debug UI, accessible from ``Profiler > Show profiler``
function M.start()
  all_samples = {}
  samples_sorted = {}
  num_samples = 0
  profile.start("li2", profiler_callback)
end

--- Stop profiling
function M.stop()
  profile.stop()
end

-- GUI --

local profiler_win_visible = false

event.update_post:bind(function()
  if imgui.begin_main_menu_bar() then
    if imgui.begin_menu("Profiler") then
      if imgui.menu_item("Start") then
        M.start()
        profiler_win_visible = true
      end
      if imgui.menu_item("Stop") then
        M.stop()
      end
      if imgui.menu_item("Show profiler", nil, profiler_win_visible) then
        profiler_win_visible = not profiler_win_visible
      end

      imgui.end_menu()
    end
  end
  imgui.end_main_menu_bar()

  if profiler_win_visible then
    if imgui.begin_win("Profiler") then
      if imgui.button("Start") then M.start() end
      imgui.same_line()
      if imgui.button("Stop") then M.stop() end
      imgui.same_line()
      imgui.text("Total samples: %d", num_samples)

      imgui.separator()

      for i = 1, math.min(100, #samples_sorted) do
        local profiled_samples = samples_sorted[i]
        local fraction = profiled_samples.samples / num_samples
        imgui.progress_bar(fraction, nil, string.format("%.0f%% (%d samples)", fraction * 100, profiled_samples.samples))

        -- Build UI
        for _, frame in ipairs(profiled_samples.stack_frames) do
          imgui.text("%s - %s", frame.func_name_short, frame.filename_and_line)

          if imgui.is_item_hovered() then
            -- Build tooltip
            local tooltip = {}
            for _, frame in ipairs(profiled_samples.stack_frames) do
              table.insert(tooltip, string.format("%s - %s", frame.func_name, frame.path_and_line))

              local line_contents = get_line(frame.path, frame.line)
              if line_contents ~= nil then
                table.insert(tooltip, line_contents)
              else
                table.insert(tooltip, "<Couldn't read file>")
              end

              table.insert(tooltip, "")
            end

            imgui.set_tooltip("%s", table.concat(tooltip, "\n"))
          end
        end
        imgui.separator()
      end
    end
    imgui.end_win()
  end
end)

return M
