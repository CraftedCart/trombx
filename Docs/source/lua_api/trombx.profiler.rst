trombx.profiler
===============

.. contents:: :local:

Description
-----------

A sampling-based profiler (this hops off of LuaJIT's profiler)

API reference
-------------

.. lua:automodule:: trombx.profiler
