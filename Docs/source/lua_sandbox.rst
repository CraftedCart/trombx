Lua sandbox
===========

Completely sandboxing Lua and LuaJIT is a tricky thing to do. We cannot guarantee 100% safety here - it's easy to miss
stuff in core Lua libraries and NLua's C# interop too, and LuaJIT has had exploits before to escape sandboxes - but a
best-effort attempt has been made to try prevent malicious code. **Always make sure when playing modcharts that they
come from a trusted source.**

TrombX does not guard against potential ways to freeze the game (like infinite loops) or unbounded memory usage. Those
are annoyances that, while not ideal, should not cause any lasting damage to a user's computer.

Changes from regular LuaJIT
---------------------------

+--------------------+-------------------------------------------------------------------------------------------------+
| Function/Library   | Behaviour                                                                                       |
+====================+=================================================================================================+
| ``collectgarbage`` | Unavailable - modcharts should not need to control garbage collection                           |
+--------------------+-------------------------------------------------------------------------------------------------+
| ``dofile``         | Unavailable - these can be used to load Lua bytecode, which is not verified and can be used as  |
+--------------------+ an attack vector                                                                                |
| ``load``           |                                                                                                 |
+--------------------+                                                                                                 |
| ``loadfile``       |                                                                                                 |
+--------------------+                                                                                                 |
| ``loadstring``     |                                                                                                 |
+--------------------+-------------------------------------------------------------------------------------------------+
| ``getfenv``        | Unavailable - modcharts should have no need to set function environments, and ``getfenv`` makes |
+--------------------+ it trivial to get the global environment and escape the sandbox                                 |
| ``setfenv``        |                                                                                                 |
+--------------------+-------------------------------------------------------------------------------------------------+
| ``module``         | Unavailable - prefer creating modules by putting Lua files that return a value into the ``lua`` |
|                    | directory of your modchart, and ``require``-ing them.                                           |
+--------------------+-------------------------------------------------------------------------------------------------+
| ``require``        | Available with restrictions                                                                     |
|                    |                                                                                                 |
|                    | - Modules loaded from your modchart will be loaded with a sandboxed environment instead of the  |
|                    |   global environment                                                                            |
|                    | - Lua bytecode modules will not be loaded                                                       |
|                    | - Trying to load TrombX private modules will raise an error                                     |
|                    | - Only the following LuaJIT built-in modules are available                                      |
|                    |    - ``bit``                                                                                    |
|                    |    - ``table.new``                                                                              |
|                    |    - ``table.clear``                                                                            |
+--------------------+-------------------------------------------------------------------------------------------------+
| ``debug``          | Mostly unavailable                                                                              |
|                    |                                                                                                 |
|                    | The entire ``debug`` library, with the exception of ``debug.traceback``, is unavailable. The    |
|                    | ``debug`` library makes it trivial to do sandbox escapes when accessing Lua internals.          |
+--------------------+-------------------------------------------------------------------------------------------------+
| ``io``             | Unavailable - modcharts should not perform filesystem access                                    |
+--------------------+-------------------------------------------------------------------------------------------------+
| ``os.date``        | Unavailable - can crash the game with bad format strings on LuaJIT                              |
+--------------------+-------------------------------------------------------------------------------------------------+
| ``os.execute``     | Unavailable - modcharts should not run arbitrary commands                                       |
+--------------------+-------------------------------------------------------------------------------------------------+
| ``os.exit``        | Unavailable - modcharts should not quit the game                                                |
+--------------------+-------------------------------------------------------------------------------------------------+
| ``os.getenv``      | Unavailable - modcharts should have not need to read environment vraiables, which could         |
|                    | potentially reveal private info about a user's machine                                          |
+--------------------+-------------------------------------------------------------------------------------------------+
| ``os.remove``      | Unavailable - modcharts should not perform filesystem access                                    |
+--------------------+                                                                                                 |
| ``os.rename``      |                                                                                                 |
+--------------------+                                                                                                 |
| ``os.tmpname``     |                                                                                                 |
+--------------------+-------------------------------------------------------------------------------------------------+
| ``os.setlocale``   | Unavailable - modifying C locale can have unintended consequences for the host program          |
+--------------------+-------------------------------------------------------------------------------------------------+
| ``package``        | Unavailable - modcharts should not change how modules are loaded, be able to load C modules,    |
|                    | access the global environment, or access TrombX internal modules                                |
+--------------------+-------------------------------------------------------------------------------------------------+
| ``string.dump``    | Unavailable - modcharts should have no need to generate bytecode when bytecode loading is also  |
|                    | unavailable                                                                                     |
+--------------------+-------------------------------------------------------------------------------------------------+
| ``jit``            | Unavailable - modcharts should have no need to control the JIT                                  |
+--------------------+-------------------------------------------------------------------------------------------------+
| ``jit.p``          | Unavailable - this needs more research as to what is safe or not to expose, however TrombX does |
+--------------------+ contain its own profiler utilizing LuaJIT's tools                                               |
| ``jit.profile``    |                                                                                                 |
+--------------------+                                                                                                 |
| ``jit.zone``       |                                                                                                 |
+--------------------+-------------------------------------------------------------------------------------------------+
| ``ffi``            | Unavailable - modcharts should not load C libraries/call into C functions                       |
+--------------------+-------------------------------------------------------------------------------------------------+
| ``string.buffer``  | Unavailable - will raise an error if trying to require this                                     |
+--------------------+-------------------------------------------------------------------------------------------------+
