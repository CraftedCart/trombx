#pragma once

// Unity needs `color` to come before `uv` when writing to a vertex buffer
// So we redefine ImDrawVert to swap some fields around
#define IMGUI_OVERRIDE_DRAWVERT_STRUCT_LAYOUT \
    struct ImDrawVert { \
        ImVec2 pos; \
        ImU32 col; \
        ImVec2 uv; \
    }
