local game_object = require("trombx.unity.game_object")
local behaviour = require("trombx.unity.behaviour")
local storage = require("trombx_private.interop.storage")
local convert = require("trombx_private.interop.convert")

local GameObject = game_object.GameObject

luanet.load_assembly("UnityEngine.CoreModule")
local UnityEngine = luanet.namespace("UnityEngine")
local UCameraClearFlags = UnityEngine.CameraClearFlags

local UCamera = UnityEngine.Camera
local URect = UnityEngine.Rect
local UColor = UnityEngine.Color

--- @class trombx.unity.camera
local M = {}

--- @alias CameraClearFlags
--- | "skybox"
--- | "solid_color"
--- | "depth"
--- | "nothing"

--- Cameras render the world
---
--- @class Camera: Behaviour
M.Camera = setmetatable({}, { __index = behaviour.Behaviour })
convert.wrapper_to_unity[M.Camera] = UCamera

--- @param lua_object Camera
--- @return UCamera
local function unwrap(lua_object)
  return storage.unwrap(lua_object) --[[@as UCamera]]
end

--- Spawn a new game object with a camera on it
---
--- @param name string|nil Optional name for the game object
---
--- @return Camera, GameObject
function M.Camera.spawn(name)
  local go = GameObject.new(name)
  local component = go:add_component(M.Camera)
  return component, go
end

--- Set the order that cameras are rendered in
---
--- Cameras with lower depth values are rendered before cameras with higher depth value
---
--- @param depth number
function M.Camera:set_depth(depth)
  unwrap(self).depth = depth
end

--- Get where in the rendering order this camera renders at
---
--- Cameras with lower depth values are rendered before cameras with higher depth value
---
--- @return number
function M.Camera:get_depth()
  return unwrap(self).depth
end

--- Set which layers the camera should render
---
--- A :lua:class:`GameObject` can be assigned a layer (with :lua:meth:`GameObject.set_layer`), and a camera's culling
--- mask is a 32-bit mask of which layers to render. For example, to render layer 31, you would need a culling mask of
--- ``1 << 31`` - or to render layers 9 and 10, you'd need a mask of ``(1 << 9) | (1 << 10)`` (look up bit shifting if
--- you're unfamiliar with this notation).
---
--- TrombX comes with `Lua BitOp <https://bitop.luajit.org/>`_, so rendering layers 9 and 10 can be expressed as such
---
--- .. code-block:: lua
---
---    local bit = require("bit")
---    camera:set_culling_mask(bit.bor(bit.lshift(1, 9), bit.lshift(1, 10)))
---
--- ...or to just render a single layer 31 for example, we can do...
---
--- .. code-block:: lua
---
---    local bit = require("bit")
---    camera:set_culling_mask(bit.lshift(1, 31))
---
--- TODO: Check if this works with u32 numbers (greater than i32 max) passed in here
---
--- @param culling_mask number The new culling mask
function M.Camera:set_culling_mask(culling_mask)
  unwrap(self).cullingMask = culling_mask
end

--- Get the layers that the camera renders
---
--- See :lua:meth:`Camera.set_culling_mask` for more info on how culling masks work
---
--- @return number
function M.Camera:get_culling_mask()
  return unwrap(self).cullingMask
end

--- Set the area on the screen that the camera renders to
---
--- This is in normalized coordinates - which means the bottom left of the screen is ``0, 0`` and the top right of the
--- screen is ``1, 1``.
---
--- @param rect Rect Where on the screen to render to
function M.Camera:set_rect(rect)
  unwrap(self).rect = URect(rect.position.x, rect.position.y, rect.size.x, rect.size.y)
end

-- TODO: get_rect

--- Set the clear color, if clear flags is set to ``solid_color``
---
--- See :lua:meth:`Camera.set_clear_flags`
---
--- @param color Rgba The background color
function M.Camera:set_background_color(color)
  unwrap(self).backgroundColor = UColor(color.r, color.g, color.b, color.a)
end
-- TODO: get_background_color

--- Set if/how the camera clears the frame before it renders
---
--- @param clear_flags CameraClearFlags How to clear the frame
function M.Camera:set_clear_flags(clear_flags)
  if clear_flags == "skybox" then
    unwrap(self).clearFlags = luanet.enum(UCameraClearFlags, "Skybox")
  elseif clear_flags == "solid_color" then
    unwrap(self).clearFlags = luanet.enum(UCameraClearFlags, "SolidColor")
  elseif clear_flags == "depth" then
    unwrap(self).clearFlags = luanet.enum(UCameraClearFlags, "Depth")
  elseif clear_flags == "nothing" then
    unwrap(self).clearFlags = luanet.enum(UCameraClearFlags, "Nothing")
  else
    error("Invalid clear flags value")
  end
end

-- TODO: get_clear_flags

return M
