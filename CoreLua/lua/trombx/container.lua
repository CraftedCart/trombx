--- @class trombx.container
local M = {}

--- An object pool, that may create objects on-the-fly if it's ever exhausted
---
--- @class DynPool
--- @field private _objects any
--- @field private _create_func fun():any
--- @field private _activate_func fun(obj:any)
--- @field private _deactivate_func fun(obj:any)
M.DynPool = {}
local DynPool_metatable = { __index = M.DynPool }

--- Create an object pool
---
--- @generic T
---
--- @param initial_size integer The initial size of the pool - ``create_func`` will be called this many times during pool creation
--- @param create_func fun():T Function called to create a new object in the pool. The object should be created in a deactivated state.
--- @param activate_func fun(obj:T) Function called when an object is taken from the pool
--- @param deactivate_func fun(obj:T) Function called when an object is added or returned to the pool
---
--- @return DynPool
function M.DynPool.new(initial_size, create_func, activate_func, deactivate_func)
  local objects = {}
  for _ = 1, initial_size do
    table.insert(objects, create_func())
  end

  return setmetatable(
    {
      _objects = objects,
      _create_func = create_func,
      _activate_func = activate_func,
      _deactivate_func = deactivate_func,
    },
    DynPool_metatable
    )
end

--- Take an object out of the pool, or if the pool is empty, create a new object and return that
---
--- @generic T
---
--- @return T
function M.DynPool:take_or_create()
  local obj
  if #self._objects > 0 then
    obj = table.remove(self._objects)
  else
    obj = self._create_func()
  end

  self._activate_func(obj)
  return obj
end

--- Add or return an object to the pool
---
--- @generic T
---
--- @param obj T The object to add into the pool
function M.DynPool:add(obj)
  self._deactivate_func(obj)
  table.insert(self._objects, obj)
end

return M
