﻿using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Rendering;

namespace DearImGuiTest
{
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public class ProcMeshTest : MonoBehaviour
    {
        [StructLayout(LayoutKind.Sequential)]
        struct Vertex
        {
            public float x;
            public float y;
            public float z;

            public float nrmX;
            public float nrmY;
            public float nrmZ;
        }

        private void OnEnable()
        {
            var mesh = new Mesh { name = "Proc Mesh" };

            mesh.SetVertexBufferParams(
                3,
                new VertexAttributeDescriptor(VertexAttribute.Position, VertexAttributeFormat.Float32, 3),
                new VertexAttributeDescriptor(VertexAttribute.Normal, VertexAttributeFormat.Float32, 3)
            );
            mesh.SetVertexBufferData(
                new Vertex[]
                {
                    new Vertex { x = 0f, y = 0f, z = 0f, nrmX = 0f, nrmY = 0f, nrmZ = 1f },
                    new Vertex { x = 1f, y = 0f, z = 0f, nrmX = 0f, nrmY = 0f, nrmZ = 1f },
                    new Vertex { x = 0f, y = 1f, z = 0f, nrmX = 0f, nrmY = 0f, nrmZ = 1f },
                },
                0,
                0,
                3
            );

            mesh.SetIndexBufferParams(3, IndexFormat.UInt16);
            mesh.SetIndexBufferData(
                new ushort[]
                {
                    0, 1, 2,
                },
                0,
                0,
                3
            );

            mesh.subMeshCount = 1; // TODO Not sure this is needed
            mesh.SetSubMesh(0, new SubMeshDescriptor(0, 3));
            mesh.RecalculateBounds(); // TODO Probably a better way

            GetComponent<MeshFilter>().mesh = mesh;

            // vertexBuffer.Dispose();
            // indexBuffer.Dispose();
        }
    }
}
