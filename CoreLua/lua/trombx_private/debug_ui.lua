local imgui = require("trombx.imgui")
local cimgui = require("trombx_private.cimgui")
local interop = require("trombx_private.interop")
local ffi = require("ffi")

local TrombX = CLRPackage("TrombX", "TrombX")

luanet.load_assembly("UnityEngine.CoreModule")
luanet.load_assembly("UnityEngine.IMGUIModule")
luanet.load_assembly("UnityEngine.InputLegacyModule")
local UnityEngine = luanet.namespace("UnityEngine")
local UnityEngineRendering = luanet.namespace("UnityEngine.Rendering")
local UnityEngineScripting = luanet.namespace("UnityEngine.Scripting")

local UScreen = UnityEngine.Screen
local UTime = UnityEngine.Time
local UVertexAttributeDescriptor = UnityEngineRendering.VertexAttributeDescriptor
local UVertexAttribute = UnityEngineRendering.VertexAttribute
local UVertexAttributeFormat = UnityEngineRendering.VertexAttributeFormat
local UIndexFormat = UnityEngineRendering.IndexFormat
local UMesh = UnityEngine.Mesh
local UMaterial = UnityEngine.Material
local UTexture2D = UnityEngine.Texture2D
local UInput = UnityEngine.Input
local UGarbageCollector = UnityEngineScripting.GarbageCollector

local get_mouse_button = UInput.GetMouseButton

local imgui_set_mesh_vertex_buffer = TrombX.ScriptEngineBridge.ImGuiSetMeshVertexBuffer
local imgui_set_mesh_index_buffer = TrombX.ScriptEngineBridge.ImGuiSetMeshIndexBuffer
local imgui_draw = TrombX.ScriptEngineBridge.ImGuiDraw

--- @class trombx_private.debug_ui
local M = {}

-- Load UI shader
local asset_bundle = TrombX.ScriptEngineBridge.TrombXAssets
assert(interop.is_valid(asset_bundle))
local imgui_shader = asset_bundle:LoadAsset("Assets/DebugUi/ImGuiShader.shader")
assert(interop.is_valid(imgui_shader))
local imgui_material = UMaterial(imgui_shader)

local white_tex = UTexture2D.whiteTexture

local imgui_context
local imgui_font_tex -- A Unity Texture2D
local IMGUI_FONT_TEX_ID = ffi.cast("void*", -2)

local mesh = UMesh()
mesh.name = "TrombX Debug UI"

local vertex_attr_position = UVertexAttributeDescriptor(
  luanet.enum(UVertexAttribute, "Position"),
  luanet.enum(UVertexAttributeFormat, "Float32"),
  2
  )
local vertex_attr_color = UVertexAttributeDescriptor(
  luanet.enum(UVertexAttribute, "Color"),
  luanet.enum(UVertexAttributeFormat, "UNorm8"),
  4
  )
local vertex_attr_uv = UVertexAttributeDescriptor(
  luanet.enum(UVertexAttribute, "TexCoord0"),
  luanet.enum(UVertexAttributeFormat, "Float32"),
  2
  )
local vertex_attrs = luanet.make_array(
  UVertexAttributeDescriptor,
  {vertex_attr_position, vertex_attr_color, vertex_attr_uv}
  )

local index_format = luanet.enum(UIndexFormat, "UInt16")

local gc_mode_disabled = luanet.enum(UGarbageCollector.Mode, "Disabled")
local gc_mode_enabled = luanet.enum(UGarbageCollector.Mode, "Enabled")
local function draw()
  if imgui.begin_main_menu_bar() then
    local gc_mode = UGarbageCollector.GCMode
    imgui.text("GC: %s", gc_mode:ToString())
    if gc_mode == gc_mode_disabled then
      if imgui.button("Enable") then
        UGarbageCollector.GCMode = gc_mode_enabled
      end
    end
  end
  imgui.end_main_menu_bar()
end

local function imgui_new_frame()
  local imgui_io = cimgui.igGetIO()

  local display_height = UScreen.height

  imgui_io.DisplaySize.x = UScreen.width
  imgui_io.DisplaySize.y = display_height
  imgui_io.DeltaTime = UTime.deltaTime

  local mouse_pos = UInput.mousePosition
  cimgui.ImGuiIO_AddMousePosEvent(imgui_io, mouse_pos.x, display_height - mouse_pos.y)
  cimgui.ImGuiIO_AddMouseButtonEvent(imgui_io, 0, get_mouse_button(0))
  cimgui.ImGuiIO_AddMouseButtonEvent(imgui_io, 1, get_mouse_button(1))

  cimgui.igNewFrame()

  draw()
end

local function imgui_render()
  cimgui.igRender()

  local draw_data = cimgui.igGetDrawData()

  for i = 0, draw_data.CmdListsCount - 1 do
    local cmd_list = draw_data.CmdLists.Data[i] -- struct ImDrawList

    -- Upload vertex and index buffers
    mesh:SetVertexBufferParams(cmd_list.VtxBuffer.Size, vertex_attrs)
    imgui_set_mesh_vertex_buffer(
      mesh,
      tonumber(ffi.cast("uintptr_t", cmd_list.VtxBuffer.Data)),
      cmd_list.VtxBuffer.Size
      )

    mesh:SetIndexBufferParams(cmd_list.IdxBuffer.Size, index_format)
    imgui_set_mesh_index_buffer(
      mesh,
      tonumber(ffi.cast("uintptr_t", cmd_list.IdxBuffer.Data)),
      cmd_list.IdxBuffer.Size
      )

    for cmd_i = 0, cmd_list.CmdBuffer.Size - 1 do
      local cmd = cmd_list.CmdBuffer.Data + cmd_i
      if cmd.UserCallback ~= nil then
        -- TODO? Do we need to support user callbacks really?
      else
        if cimgui.ImDrawCmd_GetTexID(cmd) == IMGUI_FONT_TEX_ID then
          imgui_material.mainTexture = imgui_font_tex
        else
          -- TODO: Make other textures work
          imgui_material.mainTexture = white_tex
        end

        imgui_draw(
          mesh,
          imgui_material,
          cmd.ClipRect.x,
          cmd.ClipRect.y,
          cmd.ClipRect.z,
          cmd.ClipRect.w,
          cmd.IdxOffset,
          cmd.ElemCount
          )
      end
    end
  end

  imgui_new_frame()
end

function M.init()
  imgui_context = ffi.gc(cimgui.igCreateContext(nil), cimgui.igDestroyContext)

  local imgui_io = cimgui.igGetIO()

  -- Load font atlas
  local width = ffi.new("int[1]")
  local height = ffi.new("int[1]")
  local pixels = ffi.new("unsigned char*[1]")
  cimgui.ImFontAtlas_GetTexDataAsRGBA32(imgui_io.Fonts, pixels, width, height, nil)
  imgui_font_tex = TrombX.ScriptEngineBridge.TextureFromRgba32(
    tonumber(ffi.cast("uintptr_t", pixels[0])),
    width[0],
    height[0]
    )
  cimgui.ImFontAtlas_SetTexID(imgui_io.Fonts, IMGUI_FONT_TEX_ID)

  imgui_io.ConfigFlags = cimgui.ImGuiConfigFlags_DockingEnable
  imgui_io.MouseDrawCursor = true
  imgui_io.ConfigDockingWithShift = false

  imgui_new_frame()

  TrombX.ScriptEngineBridge.EventImGuiRender:Bind(imgui_render)
end

return M
