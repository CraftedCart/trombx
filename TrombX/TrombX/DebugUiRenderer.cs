using System;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Rendering;

namespace TrombX
{
    /// <summary>
    /// Draws an ImGui UI
    ///
    /// This only really provides a way upload data to GPU buffers and draw to the screen, and the ImGui part is
    /// entirely controlled from Lua
    ///
    /// Only `public` so it can be accessed from TrombX's core Lua scripts - not intended for general consumption
    /// </summary>
    public class DebugUiRenderer : MonoBehaviour
    {
        /// <summary>
        /// Matches the layout of ImDrawVert in ImGui
        /// (which is not the same as what you'll find in Dear ImGui repo, since we've re-ordered some for TrombX since
        /// Unity needs color to come before UVs)
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct ImDrawVert
        {
            public float x;
            public float y;

            public uint color;

            public float uvX;
            public float uvY;
        }

        private void OnPostRender()
        {
            ScriptEngineBridge.EventImGuiRender.Invoke();
        }
    }
}
