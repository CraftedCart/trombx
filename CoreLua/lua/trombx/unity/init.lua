local unity_object = require("trombx.unity.unity_object")
local game_object = require("trombx.unity.game_object")
local component = require("trombx.unity.component")
local behaviour = require("trombx.unity.behaviour")
local object_transform = require("trombx.unity.object_transform")
local camera = require("trombx.unity.camera")
local asset_bundle = require("trombx.unity.asset_bundle")

--- @class trombx.unity
local M = {}

M.UnityObject = unity_object.UnityObject
M.GameObject = game_object.GameObject
M.Component = component.Component
M.Behaviour = behaviour.Behaviour
M.ObjectTransform = object_transform.ObjectTransform
M.Camera = camera.Camera
M.AssetBundle = asset_bundle.AssetBundle

return M
