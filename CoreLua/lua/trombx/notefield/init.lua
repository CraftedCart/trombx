local mod_core = require("trombx.notefield.mod.core")
local note = require("trombx.note")
local event = require("trombx.event")
local game = require("trombx.game")
local container = require("trombx.container")
local mathx = require("trombx.mathx")
local imgui = require("trombx.imgui")
local game_object = require("trombx.unity.game_object")
local interop = require("trombx_private.interop")
local storage = require("trombx_private.interop.storage")
local table_clear = require("table.clear") -- A LuaJIT extension

local TrombX = CLRPackage("TrombX", "TrombX")

luanet.load_assembly("UnityEngine.CoreModule")
local UnityEngine = luanet.namespace("UnityEngine")
local ULineRenderer = UnityEngine.LineRenderer
local UGameObject = UnityEngine.GameObject
local UMaterial = UnityEngine.Material
local UColor = UnityEngine.Color

local GameObject = game_object.GameObject
local make_vector3 = TrombX.ScriptEngineBridge.MakeVector3
local line_renderer_set_positions = TrombX.ScriptEngineBridge.LineRendererSetPositions
local get_current_beat = game.get_current_beat
local lerp = mathx.lerp
local nearly_equal = mathx.nearly_equal
local floor = math.floor
local table_insert = table.insert
local UTransform_SetLocalScale = TrombX.ScriptEngineBridge.Transform_SetLocalScale

-- Load required assets
local asset_bundle = TrombX.ScriptEngineBridge.TrombXAssets
assert(interop.is_valid(asset_bundle))

local note_line_shader = asset_bundle:LoadAsset("Assets/NotefieldX/NoteLineShader.shader")
assert(interop.is_valid(note_line_shader))
local note_line_material = UMaterial(note_line_shader)

--- @class trombx.notefield
local M = {}

--- Convert a table to a string, without using its `__tostring` metamethod if it has that
local function raw_tostring(t)
  local metatable = getmetatable(t)
  local orig_tostring = metatable.__tostring
  metatable.__tostring = nil
  local out = tostring(t)
  metatable.__tostring = orig_tostring
  return out
end

-- TODO: Pick nicer note colors
local red = UColor(1, 0, 0)
local blue = UColor(0, 0, 1)
local purple = UColor(0.5, 0, 0.5)
local yellow = UColor(1, 1, 0)
local pink = UColor(1, 0, 1)
local orange = UColor(1, 0.5, 0.5)
local gray = UColor(0.5, 0.5, 0.5)

-- Get the note color for a note that falls on the given beat
local function color_for_beat(beat)
  local beat_floor = floor(beat + 0.002)
  local beat_relative = beat - beat_floor
  if nearly_equal(beat_relative, 0, 0.002) then
    -- 4th note
    return red
  elseif nearly_equal(beat_relative, 0.5, 0.002) then
    -- 8th note
    return blue
  elseif nearly_equal(beat_relative, 0.333, 0.002) or nearly_equal(beat_relative, 0.666, 0.002) then
    -- 12th note
    return purple
  elseif nearly_equal(beat_relative, 0.25, 0.002) or nearly_equal(beat_relative, 0.75, 0.002) then
    -- 16th note
    return yellow
  elseif nearly_equal(beat_relative, 0.166, 0.002) or nearly_equal(beat_relative, 0.833, 0.002) then
    -- 24th note
    return pink
  elseif
    nearly_equal(beat_relative, 0.125, 0.002) or
    nearly_equal(beat_relative, 0.375, 0.002) or
    nearly_equal(beat_relative, 0.625, 0.002) or
    nearly_equal(beat_relative, 0.875, 0.002)
  then
    -- 32nd note
    return orange
  else
    -- TODO: Support other note colors
    return gray
  end
end

-- Used to get the Y position of points along a pitch bend
local function ease_in_out(time, begin, change, duration)
  time = time / (duration / 2)
  if time < 1 then
    return change / 2 * time * time + begin
  else
    time = time - 1
    return -change / 2 * (time * (time - 2) - 1) + begin
  end
end

-- Maps `NotefieldX` instances to some of their private members
-- We store this here instead of in the `NotefieldX` class directly to prevent making C#/CLR objects accessible at all
-- to track scripts - track code could potentially use this to break the sandbox
local NotefieldX_internals = setmetatable({}, { __mode = "k" })

local gamespace = TrombX.ScriptEngineBridge.GameController.noteholder.transform.parent.gameObject
assert(interop.is_valid(gamespace))
local target_note = gamespace.transform:Find("TargetNote")
assert(interop.is_valid(target_note))
local target_note_tf = target_note.transform

--- Set whether the original Trombone Champ notefield should be visible or not
---
--- This will be done for you if using the default scene preset (See the :lua:mod:`trombx.scene_preset` module)
---
--- If using :lua:class:`NotefieldX`, you probably want to hide the original notefield as such
---
--- .. code-block:: lua
---
---    notefield.set_original_notefield_visible(false)
---
--- @param visible boolean Whether the notefield should be visible
function M.set_original_notefield_visible(visible)
  gamespace:SetActive(visible)
end

--- @class PooledNote
--- @field package note Note
--- @field package note_go UGameObject
--- @field package line_renderer any
--- @field package notefield_x NotefieldX
local PooledNote = {}
local PooledNote_metatable = { __index = PooledNote }

-- How many times we divide up a beat within a note
-- TODO: Make this configurable on a notefield?
local subdivisions_per_beat = 64

-- How many points we have along the receptor line
local receptor_line_point_count = 64

function PooledNote.new(data)
  return setmetatable(data, PooledNote_metatable)
end

--- @param n Note
function PooledNote:set_note(n)
  self.note = n
  self.line_renderer.positionCount = n.duration * subdivisions_per_beat

  -- Color the note depending on where it falls on the beat
  local color = color_for_beat(n.beat_start)
  self.line_renderer.startColor = color
  self.line_renderer.endColor = color
end

-- Used by PooledNote:update()
-- We store these as upvalues so we can keep their allocations between function calls
local new_positions = {}
local note_mod_ctx = mod_core.NoteModContext._new()

--- Only called if this note is active (not stored away in the pool)
function PooledNote:update()
  -- Store a bunch of stuff in locals for speed, to avoid repeated table lookups
  local current_beat = get_current_beat()
  local notefield_x = self.notefield_x
  local beat_width = notefield_x._beat_width

  local n = self.note
  local beat_start = n.beat_start
  local start_x_pos = beat_start * beat_width
  local duration = n.duration
  local end_x_pos = start_x_pos + (duration * beat_width)
  local start_y_pos = n.pitch_start
  local pitch_delta = n.pitch_delta

  table_clear(new_positions)
  local point_count = duration * subdivisions_per_beat -- Number of points on this line
  for i = 1, point_count do
    local alpha = (i - 1) / ((duration * subdivisions_per_beat) - 1)
    local x_pos = lerp(start_x_pos, end_x_pos, alpha)

    local y_pos
    if pitch_delta == 0 then
      y_pos = start_y_pos
    else
      y_pos = ease_in_out(i - 1, start_y_pos, pitch_delta, point_count - 1)
    end

    -- Apply note modifiers
    note_mod_ctx.notefield = notefield_x
    note_mod_ctx.orig_position.x = x_pos - (get_current_beat() * notefield_x._beat_width) -- TODO: Respect latency settings and smooth scroll settings
    note_mod_ctx.orig_position.y = y_pos
    note_mod_ctx.orig_position.z = 0
    note_mod_ctx.position.x = note_mod_ctx.orig_position.x
    note_mod_ctx.position.y = note_mod_ctx.orig_position.y
    note_mod_ctx.position.z = note_mod_ctx.orig_position.z
    note_mod_ctx.beats_until_receptor = (beat_start + duration * ((i - 1) / (point_count - 1))) - current_beat
    for _, mod in ipairs(notefield_x._mod_stack) do
      mod:modify_note(note_mod_ctx)
    end

    -- TODO: Guard against NaN here and in other places mods are applied, I think it crashes Unity
    --       Maybe no need to if we rewrite the line renderer?

    local position = note_mod_ctx.position
    table_insert(new_positions, position.x)
    table_insert(new_positions, position.y)
    table_insert(new_positions, position.z)
  end
  line_renderer_set_positions(self.line_renderer, new_positions)
end

--- A replacement for Trombone Champ's notefield
---
--- @class NotefieldX
--- @field package _enabled boolean Is the notefield enabled?
--- @field package _note_data NoteData
--- @field package _beat_width number How many units long a single beat will be (this affects scroll speed)
--- @field package _note_draw_lower_bound number How many beats back from the receptor line we should draw notes
--- @field package _note_draw_upper_bound number How many beats forward from the receptor line we should draw notes
--- @field package _mod_stack NoteModBase[] List of modifiers to apply
--- @field package _layer number Layer that notefield game objects should live on
--- @field package _note_line_width number widthMultiplier for notes
M.NotefieldX = {}
local NotefieldX_metatable = { __index = M.NotefieldX }

--- Spawn a :lua:class:`NotefieldX` into the scene with no note data
---
--- @return NotefieldX
function M.NotefieldX.spawn()
  local notefield_go = UGameObject("NotefieldX")
  local notefield_tf = notefield_go.transform

  -- Receptor line
  local receptor_line_go = UGameObject("ReceptorLine")
  receptor_line_go.transform:SetParent(notefield_go.transform, false)
  local receptor_line_renderer = receptor_line_go:AddComponent(luanet.ctype(ULineRenderer))
  receptor_line_renderer.useWorldSpace = false
  receptor_line_renderer.positionCount = receptor_line_point_count
  receptor_line_renderer.widthMultiplier = 5

  -- TODO TEMP: Cube at cursor pos
  local receptor_go = UGameObject("Receptor")
  local receptor_tf = receptor_go.transform
  receptor_go.transform:SetParent(notefield_go.transform, false)

  local receptor_inner_go = UGameObject.CreatePrimitive(luanet.enum(UnityEngine.PrimitiveType, "Cube"))
  local receptor_inner_tf = receptor_inner_go.transform
  receptor_inner_tf.localScale = make_vector3(20, 20, 20)
  receptor_inner_tf:SetParent(receptor_tf, false)

  local note_container_go = UGameObject("NoteContainer")
  note_container_go.transform:SetParent(notefield_go.transform)

  notefield_tf.localPosition = make_vector3(-300, 0, 0) -- TODO: Check this looks good at 16:10

  -- Some NotefieldX private members are stored directly on the `notefield_x` table itself, for speed
  -- Others are stored behind `NotefieldX_internals[notefield_x]` - these fields must not be stored inline as user code
  -- could access them, fetch C# objects from them, and break the sandbox that way
  local notefield_x = setmetatable(
    {
      _enabled = true,
      _note_data = note.NoteData.new(),
      _beat_width = 300,
      _note_draw_lower_bound = 4,
      _note_draw_upper_bound = 8,
      _mod_stack = {},
      _layer = 0,
      _note_line_width = 10,
    },
    NotefieldX_metatable
    )
  local notefield_x_internals = {
    _notefield_go = notefield_go,
    _receptor_go = receptor_go,
    _receptor_inner_go = receptor_inner_go,
    _receptor_line_go = receptor_line_go,
    _receptor_line_renderer = receptor_line_renderer,
    _note_container_go = note_container_go,
    _note_pool = nil, -- Set just a few lines below
    _active_pooled_notes = {}, -- Maps visible Notes to PooledNotes
  }
  NotefieldX_internals[notefield_x] = notefield_x_internals
  notefield_x_internals._note_pool = container.DynPool.new(
    100,
    function() return notefield_x:_create_pooled_note() end,
    function(obj) obj.note_go:SetActive(true) end,
    function(obj) obj.note_go:SetActive(false) end
    )

  event.update_post:bind(function()
    if not notefield_x._enabled then return end

    notefield_x:_rebuild_notes()

    for _, pooled_note in pairs(notefield_x_internals._active_pooled_notes) do
      pooled_note:update()
    end

    -- Apply note modifiers to the receptor line
    table_clear(new_positions)
    for i = 1, receptor_line_point_count do
      note_mod_ctx.notefield = notefield_x
      note_mod_ctx.orig_position.x = 0
      note_mod_ctx.orig_position.y = -200 + (((i - 1) / (receptor_line_point_count - 1)) * 400)
      note_mod_ctx.orig_position.z = 0
      note_mod_ctx.position.x = note_mod_ctx.orig_position.x
      note_mod_ctx.position.y = note_mod_ctx.orig_position.y
      note_mod_ctx.position.z = note_mod_ctx.orig_position.z
      note_mod_ctx.beats_until_receptor = 0
      for _, mod in ipairs(notefield_x._mod_stack) do
        mod:modify_note(note_mod_ctx)
      end

      local position = note_mod_ctx.position
      table_insert(new_positions, position.x)
      table_insert(new_positions, position.y)
      table_insert(new_positions, position.z)
    end
    line_renderer_set_positions(receptor_line_renderer, new_positions)

    -- Apply note modifiers to receptor
    note_mod_ctx.notefield = notefield_x
    note_mod_ctx.orig_position.x = 0
    note_mod_ctx.orig_position.y = target_note_tf.localPosition.y
    note_mod_ctx.orig_position.z = 0
    note_mod_ctx.position.x = note_mod_ctx.orig_position.x
    note_mod_ctx.position.y = note_mod_ctx.orig_position.y
    note_mod_ctx.position.z = note_mod_ctx.orig_position.z
    note_mod_ctx.beats_until_receptor = 0
    for _, mod in ipairs(notefield_x._mod_stack) do
      mod:modify_note(note_mod_ctx)
    end
    receptor_tf.localPosition = make_vector3(
      note_mod_ctx.position.x,
      note_mod_ctx.position.y,
      note_mod_ctx.position.z
      )

    -- Debug UI
    imgui.begin_win("NotefieldX")

    -- TODO: Pos/Rot/Scale sliders?

    notefield_x._beat_width = imgui.drag_float("Beat width (scroll speed)", notefield_x._beat_width)
    imgui.set_item_tooltip("How wide a beat is in the world. Affects scroll speed.")

    imgui.text_unformatted("Mod stack")
    imgui.indent()
    for _, mod in ipairs(notefield_x._mod_stack) do
      if mod:is_imgui_inspectable() then
        if imgui.tree_node(tostring(mod) .. "###" .. raw_tostring(mod)) then
          mod:imgui_inspect()
          imgui.tree_pop()
        end
      else
        imgui.indent()
        imgui.text_unformatted(tostring(mod))
        imgui.unindent()
      end
    end
    imgui.unindent()

    imgui.end_win()
  end)

  return notefield_x
end

--- Set what notes the notefield will display
---
--- The original chart is always used for scoring - this only sets what notes are visually shown, but has no effect on
--- what notes the player does or doesn't need to hit.
---
--- @param note_data NoteData Notes to display on the notefield
function M.NotefieldX:set_notes(note_data)
  self._note_data = note_data
end

--- Display the notes of the currently playing song
---
--- Shorthand for just doing ``notefield_x:set_notes(NoteData.from_current_song())``
function M.NotefieldX:set_notes_from_current_song()
  self:set_notes(note.NoteData.from_current_song())
end

--- @package
function M.NotefieldX:_rebuild_notes()
  local internals = NotefieldX_internals[self]

  -- TODO: Respect scroll speed when computing lower/upper bounds, instead of guessing here
  --       Or maybe just allow the modcharter to adjust it themselves
  local current_beat = get_current_beat()
  local beat_lower_bound = current_beat - self._note_draw_lower_bound
  local beat_upper_bound = current_beat + self._note_draw_upper_bound

  -- Return all out-of-range notes to the pool
  for n, pooled_note in pairs(internals._active_pooled_notes) do
    if not n:is_in_range(beat_lower_bound, beat_upper_bound) then
      internals._note_pool:add(pooled_note)
      internals._active_pooled_notes[n] = nil
    end
  end

  -- Search through our notes until we find stuff around the current time
  for _, n in self._note_data:iter_notes_in_range(beat_lower_bound, beat_upper_bound) do
    -- Don't take this note if we already have it spawned
    if internals._active_pooled_notes[n] == nil then
      local pooled_note = internals._note_pool:take_or_create()
      internals._active_pooled_notes[n] = pooled_note

      pooled_note:set_note(n)
    end
  end
end

--- Create a note that goes in a ``NotefieldX`` note pool
--- @package
---
--- @return PooledNote
function M.NotefieldX:_create_pooled_note()
  local internals = NotefieldX_internals[self]

  local note_go = UGameObject("PooledNote")
  note_go.layer = self._layer
  local note_tf = note_go.transform
  note_tf:SetParent(internals._note_container_go.transform, false)

  local line_renderer = note_go:AddComponent(luanet.ctype(ULineRenderer))
  line_renderer.useWorldSpace = false
  line_renderer.widthMultiplier = self._note_line_width
  line_renderer.numCornerVertices = 8
  line_renderer.material = note_line_material
  line_renderer.material = note_line_material

  note_go:SetActive(false) -- Start deactivated

  local pooled_note = PooledNote.new{
    note = nil,
    note_go = note_go,
    line_renderer = line_renderer,
    notefield_x = self,
  }

  return pooled_note
end

--- Set the scroll speed
---
--- This works similarly to X-Mod in StepMania, in that scroll speed will depend on the current tempo of the song.
---
--- Defaults to 300 for a new :lua:class:`NotefieldX`
---
--- @param beat_width number How many units long a single beat should be
function M.NotefieldX:set_beat_width(beat_width)
  self._beat_width = beat_width
end

--- Get the scroll speed
---
--- This works similarly to X-Mod in StepMania, in that scroll speed will depend on the current tempo of the song.
---
--- Defaults to 300 for a new :lua:class:`NotefieldX`
---
--- @return number # How many units long a single beat is
function M.NotefieldX:get_beat_width()
  return self._beat_width
end

--- Set the draw distance for notes
---
--- @param lower_bound number How many beats back from the receptor line we should draw notes
--- @param upper_bound number How many beats forward from the receptor line we should draw notes
function M.NotefieldX:set_note_draw_bounds(lower_bound, upper_bound)
  self._note_draw_lower_bound = lower_bound
  self._note_draw_upper_bound = upper_bound
end

--- Set the backwards draw distance for notes
---
--- @param lower_bound number How many beats back from the receptor line we should draw notes
function M.NotefieldX:set_note_draw_lower_bound(lower_bound)
  self._note_draw_lower_bound = lower_bound
end

--- Set the forwards draw distance for notes
---
--- @param upper_bound number How many beats back from the receptor line we should draw notes
function M.NotefieldX:set_note_draw_upper_bound(upper_bound)
  self._note_draw_upper_bound = upper_bound
end

--- Add a note modifier to the end of the mod stack
---
--- Returns the modifier you pass in so you can grab a reference to the modifier and easily do stuff like such:
---
--- .. code-block:: lua
---
---    local zigzag = notefield:add_mod(mod.ModZigzag.new())
---
---    event.update_post:bind(function()
---      zigzag.magnitude = math.sin(game.get_current_beat())
---    end)
---
--- @param mod NoteModBase The modifier to add
---
--- @return NoteModBase # The modifier passed in
function M.NotefieldX:add_mod(mod)
  table.insert(self._mod_stack, mod)
  return mod
end

--- Insert a note modifier at a given position in the mod stack
---
--- Returns the modifier you pass in
---
--- @param mod NoteModBase The modifier to add
---
--- @return NoteModBase # The modifier passed in
function M.NotefieldX:insert_mod(pos, mod)
  table.insert(self._mod_stack, pos, mod)
  return mod
end

--- Remove a mod from the mod stack
---
--- @param mod NoteModBase The mod to remove. If the mod has been added multiple times, only the first instance of it will be removed.
function M.NotefieldX:remove_mod(mod)
  local mod_stack = self._mod_stack
  for i, v in ipairs(mod_stack) do
    if mod == v then
      table.remove(mod_stack, i)
      return
    end
  end
end

--- Remove all mods from the mod stack
function M.NotefieldX:remove_all_mods()
  table_clear(self._mod_stack)
end

--- Set whether the notefield should be visible and updating its notes
---
--- @param enabled boolean Whether the notefield should be enabled
function M.NotefieldX:set_enabled(enabled)
  self._enabled = enabled

  local internals = NotefieldX_internals[self]
  return internals._notefield_go:SetActive(enabled)
end

--- Get whether the notefield is enabled (visible and updating its notes)
---
--- @return boolean
function M.NotefieldX:is_enabled()
  return self._enabled
end

--- Get the :lua:class:`GameObject` containing the notefield
---
--- Note that the components and child GameObjects on a NotefieldX are not stable, and may change between versions.
---
--- @return GameObject
function M.NotefieldX:get_game_object()
  local internals = NotefieldX_internals[self]
  return storage.get_or_make_wrapped(internals._notefield_go, GameObject)
end

--- Set the layer that the notefield game objects are rendered on
---
--- See :lua:meth:`GameObject.set_layer` for more info on how layers work
---
--- @param layer number A value from 0 - 31
function M.NotefieldX:set_layer(layer)
  local internals = NotefieldX_internals[self]

  self._layer = layer
  internals._notefield_go.layer = layer
  internals._receptor_go.layer = layer
  internals._receptor_inner_go.layer = layer
  internals._receptor_line_go.layer = layer
  internals._note_container_go.layer = layer
  for _, pooled_note in pairs(internals._note_pool._objects) do
    pooled_note.note_go.layer = layer
  end
  for _, pooled_note in pairs(internals._active_pooled_notes) do
    pooled_note.note_go.layer = layer
  end
end

--- Set the scale of the receptor object (the object that follows the mouse position up and down)
---
--- @param scale Vec3 The new scale
function M.NotefieldX:set_receptor_scale(scale)
  local internals = NotefieldX_internals[self]
  UTransform_SetLocalScale(internals._receptor_go.transform, scale.x, scale.y, scale.z)
end

--- Set the width of the receptor line
---
--- The receptor line is the line that the receptor slides up and down on as you move your mouse up and down
---
--- @param width number The width of the line
function M.NotefieldX:set_receptor_line_width(width)
  local internals = NotefieldX_internals[self]
  internals._receptor_line_renderer.widthMultiplier = width
end

--- Get the width of the receptor line
---
--- The receptor line is the line that the receptor slides up and down on as you move your mouse up and down
---
--- @return number # The width of the line
function M.NotefieldX:get_receptor_line_width()
  local internals = NotefieldX_internals[self]
  return internals._receptor_line_renderer.widthMultiplier
end

--- Set the width of note lines
---
--- @param width number The width of the note lines
function M.NotefieldX:set_note_line_width(width)
  local internals = NotefieldX_internals[self]
  self._note_line_width = width
  for _, pooled_note in pairs(internals._note_pool._objects) do
    pooled_note.line_renderer.widthMultiplier = width
  end
  for _, pooled_note in pairs(internals._active_pooled_notes) do
    pooled_note.line_renderer.widthMultiplier = width
  end
end

--- Get the width of note lines
---
--- @return number # The width of the note lines
function M.NotefieldX:get_note_line_width()
  return self._note_line_width
end

return M
