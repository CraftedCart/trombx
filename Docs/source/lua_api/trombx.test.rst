trombx.test
===========

.. contents:: :local:

Description
-----------

A testing framework. You will never need this when creating modcharts, but you might find it helpful for complex charts
if you want to write unit tests for your chart.

API reference
-------------

.. lua:automodule:: trombx.test
