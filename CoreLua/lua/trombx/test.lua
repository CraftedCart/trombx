local event = require("trombx.event")
local imgui = require("trombx.imgui")

--- @class trombx.test
local M = {}

--- @alias TestStatus
--- | '"pass"' # The test passed
--- | '"fail"' # The test failed

--- @class Test
--- @field public run_func fun()

--- @class RunningTest
--- @field public name string
--- @field public test Test
--- @field public coro thread
--- @field public deferred_funcs fun()[]

--- @class TestResult
--- @field public name string
--- @field public status TestStatus
--- @field public traceback string|nil

-- Maps test names to their Test instancces
local known_tests = {}

-- List of running tests
local running_tests = {}
local current_running_test = nil

-- List of TestResult
local test_results = {}

local pass_count = 0
local fail_count = 0

local function report_test_fail(running_test, err)
  local traceback = debug.traceback(running_test.coro, err)

  fail_count = fail_count + 1
  print()
  print("==== Test " .. running_test.name .. " failed ====")
  print(traceback)

  table.insert(
    test_results,
    {
      name = running_test.name,
      status = "fail",
      traceback = traceback,
    }
    )
end

-- Returns whether cleanup succeeded
local function cleanup_test(running_test)
  local did_fail = false
  local tracebacks = ""

  for i = #running_test.deferred_funcs, 1, -1 do
    local ok, err = xpcall(running_test.deferred_funcs[i], debug.traceback)
    if not ok then
      did_fail = true
      tracebacks = tracebacks .. err .. "\n\n"
    end
  end

  if did_fail then
    fail_count = fail_count + 1

    print()
    print("==== Test cleanup " .. running_test.name .. " failed ====")
    print(tracebacks)

    table.insert(
      test_results,
      {
        name = running_test.name,
        status = "fail",
        traceback = tracebacks,
      }
      )
  end

  return not did_fail
end

local function report_test_pass(running_test)
  pass_count = pass_count + 1

  table.insert(
    test_results,
    {
      name = running_test.name,
      status = "pass",
      traceback = nil,
    }
    )
end

local function resume_all_tests()
  for i = #running_tests, 1, -1 do
    local running_test = running_tests[i]

    current_running_test = running_test
    local ok, err = coroutine.resume(running_test.coro)
    current_running_test = nil

    if not ok then
      -- Test failed
      report_test_fail(running_test, err)
      cleanup_test(running_test)
      table.remove(running_tests, i)
    elseif coroutine.status(running_test.coro) == "dead" then
      -- Test passed
      if cleanup_test(running_test) then
        report_test_pass(running_test)
      end
      table.remove(running_tests, i)
    end
  end

  -- If we're now done with tests, print results and unbind this from update_post
  if #running_tests == 0 then
    event.update_post:unbind(resume_all_tests)

    print()
    print(string.format("Test run finished: %d passed, %d failed", pass_count, fail_count))

    -- Sort results for easier viewing on the UI
    table.sort(test_results, function(a, b) return a.name < b.name end)
  end
end

--- Define a test
---
--- Example:
---
--- .. code-block:: lua
---
---    test.define("trombx.example.add_to_two", function()
---      test.assert_eq(1 + 1, 2)
---    end)
---
--- @param name string An identifier for the test
--- @param run_func fun() The test code to execute. This is run in a coroutine, and can use other methods in this module to wait for some frames.
function M.define(name, run_func)
  if known_tests[name] ~= nil then
    error("Attempt to define test '" .. name .. "' multiple times")
  end

  known_tests[name] = {
    run_func = run_func,
  }
end

--- Run a cleanup function at the end of the test run, regardless of whether the test passed or failed
---
--- Deferred functions are run in reverse order from the order they were created within a test
---
--- Even if the main body of a test passes, errors raised in deferred functions can still cause the whole test to fail.
---
--- Example:
---
--- .. code-block:: lua
---
---    test.define("trombx.example.with_cleanup", function()
---      local thing = spawn_thing()
---      test.defer(function()
---        despawn_thing(thing)
---      end)
---
---      test.assert_eq(thing.noodles, 4)
---    end)
---
--- @param func fun() The function to call at the end of the test
function M.defer(func)
  if current_running_test == nil then
    error("Not currently running a test")
  end

  table.insert(current_running_test.deferred_funcs, func)
end

--- Run all defined tests and print their results
---
--- Note that you must ``require`` all modules that contain tests you want to run beforehand
function M.run_all()
  if #running_tests > 0 then
    error("Tests are already running")
  end

  pass_count = 0
  fail_count = 0
  test_results = {}

  -- Create all tests coroutines
  for name, test in pairs(known_tests) do
    table.insert(
      running_tests,
      {
        name = name,
        test = test,
        coro = coroutine.create(test.run_func),
        deferred_funcs = {},
      }
      )
  end

  -- Start all tests
  resume_all_tests()
  event.update_post:bind(resume_all_tests)
end

--- Assert a value is of a given type
---
--- Asserts that ``type(value) == expected_type``
---
--- @param value any The value to check
--- @param expected_type string What type we expect it to be
function M.assert_type(value, expected_type)
  if type(value) ~= expected_type then
    error(string.format("test.assert_type: %s (%s) is not a %s", tostring(value), type(value), expected_type))
  end
end

--- Assert a value is nil
---
--- @param value any The value to check
function M.assert_nil(value)
  if value ~= nil then
    error(string.format("test.assert_nil: %s (%s) is not nil", tostring(value), type(value)))
  end
end

--- Assert a value is a number
---
--- Asserts that ``type(value) == "number"``
---
--- @param value any The value to check
function M.assert_number(value)
  if type(value) ~= "number" then
    error(string.format("test.assert_number: %s (%s) is not a nunber", tostring(value), type(value)))
  end
end

--- Assert a value is a table
---
--- Asserts that ``type(value) == "table"``
---
--- @param value any The value to check
function M.assert_table(value)
  if type(value) ~= "table" then
    error(string.format("test.assert_table: %s (%s) is not a table", tostring(value), type(value)))
  end
end

--- Assert that two values are equal
---
--- Raises an error if they aren't equal
---
--- @param a any The first value
--- @param b any The second value
function M.assert_eq(a, b)
  if a ~= b then
    error(string.format("test.assert_eq: %s and %s are not equal", tostring(a), tostring(b)))
  end
end

--- Assert that two numbers are nearly equal
---
--- Raises an error if they aren't nearly equal
---
--- @param a number The first number
--- @param b number The second number
--- @param tolerance number|nil How close the numbers have to be - defaults to 0.000001 if ``nil``
function M.assert_nearly_eq(a, b, tolerance)
  local mathx = require("trombx.mathx")

  tolerance = tolerance or 0.000001
  if not mathx.nearly_equal(a, b, tolerance) then
    error(string.format("test.assert_nearly_eq: %f and %f are not within %f of each other", a, b, tolerance))
  end
end

--- Suspend execution of a test until the next frame
function M.wait_next_frame()
  coroutine.yield()
end

-- GUI --

local results_win_visible = false

event.update_post:bind(function()
  if imgui.begin_main_menu_bar() then
    if imgui.begin_menu("Test") then
      if imgui.menu_item("Run all") then
        M.run_all()
        results_win_visible = true
      end
      if imgui.menu_item("Show results", nil, results_win_visible) then
        results_win_visible = not results_win_visible
      end

      imgui.end_menu()
    end
  end
  imgui.end_main_menu_bar()

  if results_win_visible then
    if imgui.begin_win("Test results") then
      imgui.text("Passed: %d, Failed: %d", pass_count, fail_count)

      for _, v in ipairs(test_results) do
        local label
        if v.status == "pass" then
          label = "[PASS] " .. v.name .. "###" .. v.name
        else
          label = "[FAIL] " .. v.name .. "###" .. v.name
        end

        if imgui.tree_node(label) then
          imgui.text_unformatted(v.traceback)
          imgui.tree_pop()
        end
      end
    end
    imgui.end_win()
  end
end)

return M
