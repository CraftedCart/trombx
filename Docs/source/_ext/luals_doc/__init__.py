import subprocess

import docutils.utils
from docutils.nodes import Node
from docutils.parsers import rst
from docutils.parsers.rst import Directive
from jinja2 import Environment, PackageLoader
from sphinx.application import Sphinx
from sphinx.environment import BuildEnvironment
from sphinx.util.docutils import SphinxDirective

from .doc_types import Item
from .models import LuaModule


def load_doc_json(app: Sphinx) -> list[Item]:
    doc_path = app.doctreedir.joinpath("luals_log/doc.json")
    with open(doc_path) as f:
        return Item.schema().loads(f.read(), many=True)


def render_template(directive: Directive, template: str, **kwargs) -> list[Node]:
    # Render RST using Jinja
    env = Environment(loader=PackageLoader("luals_doc", "templates"))
    template = env.get_template(template)
    rendered_rst = template.render(**kwargs)

    doc = docutils.utils.new_document("module", settings=directive.state.document.settings)
    rst.Parser().parse(rendered_rst, doc)
    return doc.children


def make_directive_automodule(app: Sphinx) -> type[Directive]:
    class AutoModuleDirective(SphinxDirective):
        has_content = True

        def run(self) -> list[Node]:
            lua_doc = load_doc_json(app)

            for item in lua_doc:
                if item.is_module() and item.name == str(self.content[0]):
                    return render_template(self, "module.rst.jinja", model=LuaModule.from_luals(lua_doc, item))

            raise Exception(f"Module not found {self.content[0]}")

    return AutoModuleDirective


def gather_luals_docs(app: Sphinx) -> None:
    luals_log_dir = app.doctreedir.joinpath("luals_log")

    # Run LuaLS to gather docs
    subprocess.run(
        [
            app.config.luals_lsp_path,
            "--logpath",
            str(luals_log_dir),
            "--doc",
            str(app.confdir.joinpath(app.config.luals_lua_path)),
        ],
        check=True,
    )


def read_all_docs(app: Sphinx, env: BuildEnvironment, doc_names: list[str]) -> None:
    # Make sure we read all docs, since we don't have any way to tell when Lua source files are changed - so just make
    # sure we rebuild all docs :P
    doc_names[:] = env.found_docs


def setup(app: Sphinx) -> dict:
    app.setup_extension("sphinxcontrib.luadomain")

    app.add_config_value("luals_lsp_path", None, "env")
    app.add_config_value("luals_lua_path", None, "env")

    app.connect("builder-inited", gather_luals_docs)
    app.connect("env-before-read-docs", read_all_docs)

    app.add_directive_to_domain("lua", "automodule", make_directive_automodule(app))

    return {
        "version": "0.1",
        "parallel_read_safe": True,
        "parallel_write_safe": True,
    }
