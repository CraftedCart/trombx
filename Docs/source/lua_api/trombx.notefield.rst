trombx.notefield
================

.. contents:: :local:

Description
-----------

Contains a replacement for Trombone Champ's notefield, which makes it easier to implement effects involving note visuals
(eg: changing scroll speeds, moving notes around)

Note modifiers
^^^^^^^^^^^^^^

Modifiers can be applied to the playfield, affecting visuals such as the paths that notes take to the receptor, or the
colors of notes (TODO).

Note that the order in which modifiers are applied can matter for some modifiers - modifiers that are applied
first-to-last, with later-on modifiers being given the output of earlier modifiers to work with.

Example: Creating a NotefieldX, applying a couple modifiers, and changing scroll speed
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: lua

   local notefield = require("trombx.notefield")
   local note = require("trombx.note")
   local mod = require("trombx.notefield.mod")

   -- Hide the original Trombone Champ notefield
   notefield.set_original_notefield_visible(false)

   -- Create a NotefieldX using the notes of the current song
   local notefield_x = notefield.NotefieldX.spawn()
   notefield_x:set_notes(note.NoteData.from_current_song())

   -- Slow down the scroll speed from the default 300 to 50
   notefield_x:set_beat_width(50)

   -- Add some modifiers to the notefield
   notefield_x:add_mod(mod.ModZigzag.new(20, 0.5))
   notefield_x:add_mod(mod.ModDecelerate.new(2))

Example: Changing a modifier over time
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: lua

   local game = require("trombx.game")
   local event = require("trombx.event")
   local notefield = require("trombx.notefield")
   local note = require("trombx.note")
   local mod = require("trombx.notefield.mod")

   -- Hide the original Trombone Champ notefield
   notefield.set_original_notefield_visible(false)

   -- Create a NotefieldX using the notes of the current song
   local notefield_x = notefield.NotefieldX.spawn()
   notefield_x:set_notes(note.NoteData.from_current_song())

   -- Add a zigzag modifier to the notefield
   local zigzag = notefield_x:add_mod(mod.ModZigzag.new(4))

   -- Set the zigzag intensity to the sine of time
   event.update_post:bind(function()
     zigzag.magnitude = math.sin(game.get_current_beat()) * 50
   end)

API reference
-------------

.. lua:automodule:: trombx.notefield
