from dataclasses import dataclass, field
from typing import Optional

from dataclasses_json import dataclass_json, Undefined, config


@dataclass_json(undefined=Undefined.EXCLUDE)
@dataclass(frozen=True)
class Arg:
    type: str
    view: str
    start: int
    finish: int
    name: str = "???"
    desc: Optional[str] = None
    rawdesc: Optional[str] = None

    def is_vararg(self) -> bool:
        return self.type == "..."


@dataclass_json(undefined=Undefined.EXCLUDE)
@dataclass(frozen=True)
class Return:
    type: str
    view: str
    name: str = "???"
    desc: Optional[str] = None
    rawdesc: Optional[str] = None


@dataclass_json(undefined=Undefined.EXCLUDE)
@dataclass(frozen=True)
class Type:
    type: str
    view: str
    start: int
    finish: int


@dataclass_json(undefined=Undefined.EXCLUDE)
@dataclass(frozen=True)
class Extends:
    type: str
    view: str
    start: int
    finish: int
    args: list[Arg] = field(default_factory=list)
    returns: list[Return] = field(default_factory=list)
    types: list[Type] = field(default_factory=list)


def decode_single_or_multiple_extends(item) -> list[Extends]:
    if isinstance(item, dict):
        return [Extends.schema().load(item)]
    elif isinstance(item, list):
        return Extends.schema().load(item, many=True)
    else:
        raise TypeError("Unexpected type for extends")


@dataclass_json(undefined=Undefined.EXCLUDE)
@dataclass(frozen=True)
class Define:
    file: str
    start: int
    finish: int
    type: str
    extends: list[Extends] = field(default_factory=list, metadata=config(decoder=decode_single_or_multiple_extends))


@dataclass_json(undefined=Undefined.EXCLUDE)
@dataclass(frozen=True)
class Field:
    name: str
    type: str
    extends: Extends
    file: str
    start: int
    finish: int
    desc: Optional[str] = None
    rawdesc: Optional[str] = None

    def is_class_ref(self):
        return (
                self.extends.type == "table" or
                self.extends.view == "table" or
                self.extends.view.startswith("table|")
        ) and self.name[0].isupper()

    def is_function(self):
        return self.extends.type == "function"

    def is_variable(self):
        return (not self.is_re_export() and
                (((self.type == "doc.field" and self.extends.type == "doc.type") or
                  (self.type == "setfield" and self.extends.type != "function"))))

    def is_re_export(self):
        return self.type == "setfield" and self.extends.type == "getfield"

    def is_private(self):
        # Heuristic-y, unfortunately LuaLS doesn't give us private/package/protected/public states of anything
        return self.name[0] == "_"


@dataclass_json(undefined=Undefined.EXCLUDE)
@dataclass(frozen=True)
class Item:
    name: str
    type: str
    defines: list[Define]
    fields: list[Field] = field(default_factory=list)
    desc: Optional[str] = None
    rawdesc: Optional[str] = None

    def is_module(self):
        # A little ugly heuristic-y, but it works well enough
        return self.type == "type" and self.name[0].islower()

    def is_class(self):
        # A little ugly heuristic-y, but it works well enough
        return self.type == "type" and self.name[0].isupper()
