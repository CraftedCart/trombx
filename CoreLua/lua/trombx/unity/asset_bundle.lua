local unity_object = require("trombx.unity.unity_object")
local game_object = require("trombx.unity.game_object")
local storage = require("trombx_private.interop.storage")
local convert = require("trombx_private.interop.convert")
local interop = require("trombx_private.interop")

local SystemIO = CLRPackage("System.Runtime", "System.IO")
local TrombX = CLRPackage("TrombX", "TrombX")

luanet.load_assembly("UnityEngine.CoreModule")
luanet.load_assembly("UnityEngine.AssetBundleModule")
local UnityEngine = luanet.namespace("UnityEngine")

local GameObject = game_object.GameObject
local UAssetBundle = UnityEngine.AssetBundle
local UGameObject = UnityEngine.GameObject

--- @class trombx.unity.asset_bundle
local M = {}

--- Asset bundles let you load Unity assets from disk, such as prefabs and shaders
---
--- @class AssetBundle: UnityObject
M.AssetBundle = setmetatable({}, { __index = unity_object.UnityObject })
convert.wrapper_to_unity[M.AssetBundle] = UAssetBundle

--- Keeps track of loaded asset bundles, so we can unload them all when the Lua state dies
---
--- @type userdata[]
local loaded_proxies = {}

--- @param lua_object AssetBundle
--- @return UAssetBundle
local function unwrap(lua_object)
  return storage.unwrap(lua_object) --[[@as UAssetBundle]]
end

--- Load an asset bundle from your modchart's directory
---
--- Important: This automatically suffixes the path passed in with `_win` or `_mac` for the Windows and macOS builds of
--- the game respectively. You should not add these suffixes yourself.
---
--- Returns ``nil`` if loading the asset bundle failed
---
--- @param path string Path of the asset bundle, relative to the modchart's directory
---
--- @return AssetBundle|nil
function M.AssetBundle.load(path)
  -- A kinda lazy attempt at preventing directory traversal
  if path:find("%.%.") ~= nil then
    error("Invalid asset bundle path")
  end

  if jit.os == "Windows" then
    path = path .. "_win"
  elseif jit.os == "OSX" then
    path = path .. "_mac"
  else
    error("Unknown OS?")
  end

  local current_track_dir = TrombX.ScriptEngineBridge.GetCurrentModTrackDirectory()
  local path = SystemIO.Path.GetFullPath(SystemIO.Path.Combine(current_track_dir, path))
  local bundle = UAssetBundle.LoadFromFile(path, 0, 0)
  if interop.is_valid(bundle) then
    -- Unload the asset bundle when the Lua state is stopped
    local gc_proxy = newproxy(true)
    local gc_proxy_metatable = debug.getmetatable(gc_proxy)
    gc_proxy_metatable.__gc = function()
      bundle:Unload(true)
    end
    table.insert(loaded_proxies, gc_proxy)

    return storage.get_or_make_wrapped(bundle, M.AssetBundle)
  else
    -- Loading the bundle failed
    return nil
  end
end

--- Load a prefab from the asset bundle
---
--- @param name string Name of the object to load
---
--- @return GameObject|nil
function M.AssetBundle:load_game_object(name)
  local asset = unwrap(self):LoadAsset(name, luanet.ctype(UGameObject))
  if interop.is_valid(asset) then
    return storage.get_or_make_wrapped(asset, GameObject)
  else
    return nil
  end
end

return M
