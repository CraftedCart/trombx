local unity_object = require("trombx.unity.unity_object")
local object_transform = require("trombx.unity.object_transform")
local test = require("trombx.test")
local storage = require("trombx_private.interop.storage")
local convert = require("trombx_private.interop.convert")

luanet.load_assembly("UnityEngine.CoreModule")
local UnityEngine = luanet.namespace("UnityEngine")
local TrombX = CLRPackage("TrombX", "TrombX")

local ObjectTransform = object_transform.ObjectTransform

local UGameObject = UnityEngine.GameObject
local UGameObject_GetTransform = TrombX.ScriptEngineBridge.GameObject_GetTransform
local UGameObject_SetActive = TrombX.ScriptEngineBridge.GameObject_SetActive
local UGameObject_IsActiveSelf = TrombX.ScriptEngineBridge.GameObject_IsActiveSelf
local UGameObject_IsActiveInHierarchy = TrombX.ScriptEngineBridge.GameObject_IsActiveInHierarchy

--- @class trombx.unity.game_object
local M = {}

--- An object in the scene
---
--- @class GameObject: UnityObject
M.GameObject = setmetatable({}, { __index = unity_object.UnityObject })
convert.wrapper_to_unity[M.GameObject] = UGameObject

--- @param lua_object GameObject
--- @return UGameObject
local function unwrap(lua_object)
  return storage.unwrap(lua_object) --[[@as UGameObject]]
end

--- Spawn an empty game object into the scene
---
--- @param name string|nil Optional name of the object
---
--- @return GameObject
function M.GameObject.new(name)
  local unity_go = UGameObject(name)
  return storage.get_or_make_wrapped(unity_go, M.GameObject)
end

--- Add a component to a game object
---
--- **Example:**
---
--- .. code-block:: lua
---
---    local unity = require("trombx.unity")
---
---    local game_object = unity.GameObject.new()
---    local camera = game_object:add_component(unity.Camera)
---
--- @generic T: Component
---
--- @param ty T The type of component to add
---
--- @return T
function M.GameObject:add_component(ty)
  local unity_ty = convert.wrapper_to_unity[ty]
  if unity_ty == nil then
    error("Not a component type")
  end

  local unity_component = unwrap(self):AddComponent(luanet.ctype(unity_ty))
  return storage.get_or_make_wrapped(unity_component, ty)
end

--- Returns the GameObject's name
---
--- @return string
function M.GameObject:get_name()
  return unwrap(self).name
end

--- Set the GameObject's name
---
--- @param name string The new name
function M.GameObject:set_name(name)
  unwrap(self).name = name
end

--- Returns the GameObject's transform
---
--- @return ObjectTransform
function M.GameObject:get_transform()
  return storage.get_or_make_wrapped(UGameObject_GetTransform(unwrap(self)), ObjectTransform)
end

--- Activate or deactivate a GameObject
---
--- See https://docs.unity3d.com/ScriptReference/GameObject.SetActive.html
---
--- @param active boolean The new active state
function M.GameObject:set_active(active)
  UGameObject_SetActive(unwrap(self), active)
end

--- Returns whether the GameObject wants to be active
---
--- Returns the active state of the GameObject, which is the same as whatever is passed into :lua:meth:`set_active`.
--- Note that a GameObject may be inactive even if this returns ``true``, if any of its parent GameObjects are inactive.
---
--- See https://docs.unity3d.com/ScriptReference/GameObject-activeSelf.html
---
--- @return boolean
function M.GameObject:is_active_self()
  return UGameObject_IsActiveSelf(unwrap(self))
end

--- Returns whether the GameObject is active
---
--- Returns ``true`` if the GameObject itself and all parents are all active.
---
--- See https://docs.unity3d.com/ScriptReference/GameObject-activeInHierarchy.html
---
--- @return boolean
function M.GameObject:is_active_in_hierarchy()
  return UGameObject_IsActiveInHierarchy(unwrap(self))
end

--- Set the layer that the game object is rendered on
---
--- You can use this in conjunction with :lua:meth:`Camera.set_culling_mask` to only render certain objects through
--- certain cameras.
---
--- Unity reserves the first 8 layers for itself (0 - 7), and Trombone Champ uses layer 8 for some UI as of writing
--- this. It's advised to avoid these using layers in modcharts when wanting to selectively render objects, perhaps
--- starting at the end with layer 31 and working backwards from there for more layers.
---
--- @param layer number A value from 0 - 31
function M.GameObject:set_layer(layer)
  unwrap(self).layer = layer
end

--- Spawn an empty GameObject for use in tests
---
--- Will automatically destroy the GameObject when the current test finishes
---
--- @param name string|nil
---
--- @return UGameObject, GameObject
local function spawn_test_game_object(name)
  local interop = require("trombx_private.interop")
  luanet.load_assembly("UnityEngine.CoreModule")
  local UnityEngine = luanet.namespace("UnityEngine")

  local unity_go = UnityEngine.GameObject(name)
  local wrapped = storage.get_or_make_wrapped(unity_go, M.GameObject)

  test.defer(function()
    if interop.is_unity_object_valid(unity_go) then
      UnityEngine.Object.Destroy(unity_go)
    end
  end)

  return unity_go, wrapped
end

test.define("trombx.unity.game_object.GameObject.is_valid", function()
  luanet.load_assembly("UnityEngine.CoreModule")
  local UnityEngine = luanet.namespace("UnityEngine")

  local unity_go, wrapped = spawn_test_game_object()

  assert(wrapped:is_valid())

  UnityEngine.Object.Destroy(unity_go)
  test.wait_next_frame()

  assert(not wrapped:is_valid())
end)

test.define("trombx.unity.game_object.GameObject.active_state", function()
  local parent_unity_go, parent_wrapped = spawn_test_game_object()
  local child_unity_go, child_wrapped = spawn_test_game_object()
  child_unity_go.transform:SetParent(parent_unity_go.transform)

  child_wrapped:set_active(false)
  assert(parent_wrapped:is_active_self())
  assert(parent_wrapped:is_active_in_hierarchy())
  assert(not child_wrapped:is_active_self())
  assert(not child_wrapped:is_active_in_hierarchy())

  child_wrapped:set_active(true)
  assert(parent_wrapped:is_active_self())
  assert(parent_wrapped:is_active_in_hierarchy())
  assert(child_wrapped:is_active_self())
  assert(child_wrapped:is_active_in_hierarchy())

  parent_wrapped:set_active(false)
  assert(not parent_wrapped:is_active_self())
  assert(not parent_wrapped:is_active_in_hierarchy())
  assert(child_wrapped:is_active_self())
  assert(not child_wrapped:is_active_in_hierarchy())
end)

test.define("trombx.unity.game_object.GameObject.name", function()
  local _, go = spawn_test_game_object("TrombXTestGameObject")
  test.assert_eq(go:get_name(), "TrombXTestGameObject")

  go:set_name("AnotherName")
  test.assert_eq(go:get_name(), "AnotherName")
end)

test.define("trombx.unity.game_object.GameObject.new.nil_name", function()
  local go = M.GameObject.new()
  test.defer(function() go:destroy() end)
  assert(go:is_valid())
  test.assert_eq(go:get_name(), "New Game Object")
end)

test.define("trombx.unity.game_object.GameObject.new.with_name", function()
  local go = M.GameObject.new("TestObjectName")
  test.defer(function() go:destroy() end)
  assert(go:is_valid())
  test.assert_eq(go:get_name(), "TestObjectName")
end)

return M
