trombx.unity.object_transform
=============================

.. contents:: :local:

Description
-----------

The transform of a GameObject

API reference
-------------

.. lua:automodule:: trombx.unity.object_transform
