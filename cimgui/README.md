TrombX's cimgui
===============

TrombX uses a slightly customized build of Dear ImGui/cimgui - specifically changing the layout of the vertex struct
ImGui generates, to fit Unity's vertex buffer layout requirements. To build on Windows, run `generate.bat` (to
regenerate C binding code), followed by `build.bat` (to build Dear ImGui/cimgui with CMake) in a Developer Command
Prompt.

TODO: macOS support
