local mod_core = require("trombx.notefield.mod.core")
local mod_zigzag = require("trombx.notefield.mod.zigzag")
local mod_decelerate = require("trombx.notefield.mod.decelerate")

--- @class trombx.notefield.mod
local M = {}

M.NoteModContext = mod_core.NoteModContext
M.NoteModBase = mod_core.NoteModBase
M.ModZigzag = mod_zigzag.ModZigzag
M.ModDecelerate = mod_decelerate.ModDecelerate

return M
