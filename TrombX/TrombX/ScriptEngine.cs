using System;
using System.IO;
using BepInEx;
using NLua;
using NLua.Exceptions;

namespace TrombX
{
    public class ScriptEngine : IDisposable
    {
        internal readonly Lua Lua = new();

        internal ScriptEngine()
        {
            Lua.UseTraceback = true;
            Lua.LoadCLRPackage();
        }

        /// <summary>
        /// Loads TrombX init script and track init script (the track init script is loaded from the TrombX init script)
        /// </summary>
        internal void LoadTrackScripts()
        {
            var initScriptPath = Path.Combine(Paths.PluginPath, "TrombX", "init.lua");
            TryDoFile(initScriptPath);
        }

        /// <summary>
        /// Tries to execute a file. Does not throw on error
        /// </summary>
        private void TryDoFile(string path)
        {
            try
            {
                Lua.DoFile(path);
            }
            catch (LuaScriptException e)
            {
                TrombXPlugin.Instance.Logger.LogError("Failed loading script");
                TrombXPlugin.Instance.Logger.LogError(e);
                TrombXPlugin.Instance.Logger.LogError(e.StackTrace);

                if (e is { IsNetException: true, InnerException: not null })
                {
                    TrombXPlugin.Instance.Logger.LogError(e.InnerException);
                    TrombXPlugin.Instance.Logger.LogError(e.InnerException.StackTrace);
                }
            }
        }

        public void Dispose()
        {
            Lua.Dispose();
        }
    }
}
