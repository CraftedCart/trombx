local notefield = require("trombx.notefield")
local unity = require("trombx.unity")
local storage = require("trombx_private.interop.storage")

luanet.load_assembly("UnityEngine.CoreModule")
luanet.load_assembly("UnityEngine.UIModule")
luanet.load_assembly("UnityEngine.SpriteMaskModule")
local UnityEngine = luanet.namespace("UnityEngine")

local UVector3 = UnityEngine.Vector3
local UColor = UnityEngine.Color
local UGameObject = UnityEngine.GameObject
local UCamera = UnityEngine.Camera
-- local USpriteMask = UnityEngine.SpriteMask
-- local USpriteRenderer = UnityEngine.SpriteRenderer
local UCanvas = UnityEngine.Canvas
local UCameraClearFlags = UnityEngine.CameraClearFlags
local UCameraClearFlags_SolidColor = luanet.enum(UCameraClearFlags, "SolidColor")
local UCameraClearFlags_Nothing = luanet.enum(UCameraClearFlags, "Nothing")

-- Culling masks
-- The first 8 (bits 0 - 7) are reserved for Unity
-- You can find out these names running `LayerMask.LayerToName` in a packaged build
-- Bit  0 (value          1): "Default"
-- Bit  1 (value          2): "TransparentFX"
-- Bit  2 (value          4): "Ignore Raycast"
-- Bit  3 (value          8): ""
-- Bit  4 (value         16): "Water"
-- Bit  5 (value         32): "UI" - Has CHAMP meter
-- Bit  6 (value         64): ""
-- Bit  7 (value        128): ""
-- Bit  8 (value        256): "LevelEditor" - Has the rest of the UI
-- Bit  9 (value        512): ""
-- Bit 10 (value       1024): ""
-- Bit 11 (value       2048): ""
-- Bit 12 (value       4096): ""
-- Bit 13 (value       8192): ""
-- Bit 14 (value      16384): ""
-- Bit 15 (value      32768): ""
-- Bit 16 (value      65536): ""
-- Bit 17 (value     131072): ""
-- Bit 18 (value     262144): ""
-- Bit 19 (value     524288): ""
-- Bit 20 (value    1048576): ""
-- Bit 21 (value    2097152): ""
-- Bit 22 (value    4194304): ""
-- Bit 23 (value    8388608): ""
-- Bit 24 (value   16777216): ""
-- Bit 25 (value   33554432): ""
-- Bit 26 (value   67108864): ""
-- Bit 27 (value  134217728): ""
-- Bit 28 (value  268435456): ""
-- Bit 29 (value  536870912): ""
-- Bit 30 (value 1073741824): ""
-- Bit 31 (value 2147483648): ""

--- @class trombx.scene_preset
local M = {}

--- @class DefaultPreset
--- @field main_camera Camera public Camera positioned at ``0, 0, 300`` with a low FOV. Spawning a :lua:class:`NotefieldX` in the default position will be positioned well for this camera.
M.DefaultPreset = {}
local DefaultPreset_metatable = { __index = M.DefaultPreset }

--- Setup the scene to a near-blank state
---
--- Automatically disable some in-game models, the notefield, lighting, cameras, etc. and gives you a near-blank slate
--- to start adding your own content into the scene. The in-game UI is kept.
---
--- @return DefaultPreset
function M.DefaultPreset.apply()
  notefield.set_original_notefield_visible(false)

  -- Disable some default cameras
  UGameObject.Find("BGCam_ballgame(Clone)"):SetActive(false)
  UGameObject.Find("3dModelCamera"):SetActive(false)
  UGameObject.Find("GameplayCam"):SetActive(false)

  -- Disable background objects
  -- (TootTally's background dim also goes here and gets disabled)
  UGameObject.Find("BGCameraObj"):SetActive(false)

  -- Hide the tromboner
  UGameObject.Find("PlayerModelHolder"):SetActive(false)

  -- The rainbow camera sometimes just gets in the way, we don't need it
  -- Just setting in inactive breaks things when you fill the CHAMP meter though - we'll disable just the camera
  -- component on it
  UGameObject.Find("RainbowCamera"):GetComponent(luanet.ctype(UCamera)).enabled = false

  -- Disable default lighting
  UGameObject.Find("Directional Light"):SetActive(false)

  -- Move the health meter (the trombone behind the CHAMP meter) to only be visible on the UI
  -- TODO: This doesn't work
  -- UGameObject.Find("HealthMask/mask"):GetComponent(luanet.ctype(USpriteMask)).renderingLayerMask = 32
  -- UGameObject.Find("HealthMask/BG"):GetComponent(luanet.ctype(USpriteRenderer)).renderingLayerMask = 32
  -- UGameObject.Find("HealthMask/HealthFill/HealthFill"):GetComponent(luanet.ctype(USpriteRenderer)).renderingLayerMask = 32
  -- UGameObject.Find("HealthMask/HealthFill/HealthFill2"):GetComponent(luanet.ctype(USpriteRenderer)).renderingLayerMask = 32

  -- Just hide the health meter
  UGameObject.Find("HealthMask"):SetActive(false)

  local main_cam_go = UGameObject("TrombX Main Camera")
  local main_cam_tf = main_cam_go.transform
  main_cam_tf.localPosition = UVector3(0, 0, -800)

  local main_cam = main_cam_go:AddComponent(luanet.ctype(UCamera))
  main_cam.clearFlags = UCameraClearFlags_SolidColor
  main_cam.backgroundColor = UColor.black
  -- main_cam.cullingMask = -33 -- All bits except the CHAMP meter bit (32)
  main_cam.fieldOfView = 30
  main_cam.farClipPlane = 1000000

  -- TODO: Maybe hide HealthMask and manually send draw commands in the UI camera here to render it?
  local ui_cam_go = UGameObject("TrombX UI Camera")
  local ui_cam = ui_cam_go:AddComponent(luanet.ctype(UCamera))
  ui_cam.clearFlags = UCameraClearFlags_Nothing
  ui_cam.cullingMask = 32 + 256 -- Draw only UI
  ui_cam.orthographic = true
  ui_cam.depth = 25

  -- GameplayCanvas usually goes to GameplayCam, which we just disabled above
  -- Same with ChampCanvas
  UGameObject.Find("GameplayCanvas"):GetComponent(luanet.ctype(UCanvas)).worldCamera = ui_cam
  UGameObject.Find("ChampCanvas"):GetComponent(luanet.ctype(UCanvas)).worldCamera = ui_cam

  return setmetatable(
    {
      main_camera = storage.get_or_make_wrapped(main_cam, unity.Camera),
    },
    DefaultPreset_metatable
    )
end

return M
