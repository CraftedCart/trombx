local TrombX = CLRPackage("TrombX", "TrombX")

--- @class trombx_private.interop
local M = {}

M.is_unity_object_valid = TrombX.ScriptEngineBridge.IsObjectValid

-- Null checks might be hot so let's optimize not having to do table lookups by storing IsObjectValid in a local here
local is_unity_object_valid = TrombX.ScriptEngineBridge.IsObjectValid

--- Check if a given object is non-nil
---
--- When working with Unity objects (``UnityEngine.Object`` subclasses), this should be preferred to check whether
--- objects are valid. Doing regular ``nil`` checks in Lua does not always work for for Unity objects, due to the need
--- for Unity objects to interoperate with the underlying C++ engine, and how Unity overrides equality operators to have
--- special logic for null checking.
---
--- This still works when not passed a Unity object, in which case it will do a regular Lua ``nil`` comparison.
---
--- @param obj any The object to check
---
--- @return boolean
function M.is_valid(obj)
  if obj == nil then return false end

  if type(obj) == "userdata" then
    return is_unity_object_valid(obj)
  else
    return true
  end
end

return M
