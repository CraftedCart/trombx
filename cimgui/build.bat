setlocal

REM Make and cd into a `build` dir
cd /D "%~dp0"
if not exist "build" mkdir "build"
cd build

REM Configure and build with CMake
REM Yes, the escaping here is ridiculous
set "IncludeDir=%~dp0\include"
cmake "-DCMAKE_CXX_FLAGS=/I\""%IncludeDir:\=/%\"" /DIMGUI_USER_CONFIG=""\\\""TrombX_imconfig.hpp\\\""""" "%~dp0\cimgui"
cmake --build . --config RelWithDebInfo