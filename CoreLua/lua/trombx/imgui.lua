local cimgui = require("trombx_private.cimgui")
local ffi = require("ffi")

--- @class trombx.imgui
local M = {}

--- @class ImVec2
--- @field public x number
--- @field public y number

-- local FLT_MIN = cimgui.igGET_FLT_MIN()
local FLT_MAX = cimgui.igGET_FLT_MAX()

--- Begin a window
---
--- Example:
---
--- .. code-block:: lua
---
---    if imgui.begin_win("A window") then
---      imgui.text_unformatted("Hello, world!")
---    end
---    imgui.end_win()
---
--- @param name string The name shown in the title of the window
---
--- @return boolean # Whether the window contents are visible
function M.begin_win(name)
  return cimgui.igBegin(name, nil, cimgui.ImGuiWindowFlags_None)
end

--- End a window
---
--- Should always be called to end a window, regardless of whatever :lua:meth:`begin_win` returned.
function M.end_win()
  cimgui.igEnd()
end

--- Draw a label
---
--- @param format string Format string passed to Lua's ``string.format``
--- @param ... any Args to pass to ``string.format``
function M.text(format, ...)
  cimgui.igTextUnformatted(string.format(format, ...), nil)
end

--- Draw a label with the disabled style
---
--- @param format string Format string passed to Lua's ``string.format``
--- @param ... any Args to pass to ``string.format``
function M.text_disabled(format, ...)
  cimgui.igTextDisabled("%s", string.format(format, ...))
end

--- Draw a label, without going through ``string.format``
---
--- @param text string
function M.text_unformatted(text)
  cimgui.igTextUnformatted(text, nil)
end

--- Indent further content
---
--- @param indent_w number|nil How far right to move. If ``nil`` or ``<= 0``, indent by the IndentSpacing style value.
function M.indent(indent_w)
  indent_w = indent_w or 0
  cimgui.igIndent(indent_w)
end

--- Unindent further content
---
--- @param indent_w number|nil How far right to move. If ``nil`` or ``<= 0``, indent by the IndentSpacing style value.
function M.unindent(indent_w)
  indent_w = indent_w or 0
  cimgui.igUnindent(indent_w)
end

--- Call between widgets or groups to layout them horizontally
---
--- X position given in window coordinates.
---
--- @param offset_from_start_x number|nil
--- @param spacing number|nil Spacing between widgets
function M.same_line(offset_from_start_x, spacing)
  offset_from_start_x = offset_from_start_x or 0
  spacing = spacing or -1
  cimgui.igSameLine(offset_from_start_x, spacing)
end

-- TODO: Size param for the button

--- A button
---
--- Returns ``true`` when clicked on
---
--- Example:
---
--- .. code-block:: lua
---
---    if imgui.button("Click me") then
---      print("Clicked!")
---    end
---
--- @param label string Text on the button
---
--- @return boolean # True on the frame the button was clicked on
function M.button(label)
  local size = ffi.new("ImVec2") --[[@as ImVec2]]
  size.x = 0
  size.y = 0
  return cimgui.igButton(label, size)
end

--- A small button
---
--- Returns ``true`` when clicked on
---
--- Example:
---
--- .. code-block:: lua
---
---    if imgui.small_button("Click me") then
---      print("Clicked!")
---    end
---
--- @param label string Text on the button
---
--- @return boolean # True on the frame the button was clicked on
function M.small_button(label)
  return cimgui.igSmallButton(label)
end

--- A number input field, that can be dragged on to adjust
---
--- Example:
---
--- .. code-block:: lua
---
---    intensity, was_dragged = imgui.drag_float("Intensity", intensity)
---    -- Or just...
---    intensity = imgui.drag_float("Intensity", intensity)
---
--- @param label string Label to show on the slider
--- @param value number Current value of the slider
--- @param v_speed number|nil How sensitive dragging is. This is how much the value changes by per pixel of mouse movement. 1 if ``nil``.
--- @param v_min number|nil ``-FLT_MAX`` if ``nil``
--- @param v_max number|nil ``FLT_MAX`` if ``nil``
---
--- @return number,boolean # The new value of the slider, and whether the slider was interacted with
function M.drag_float(label, value, v_speed, v_min, v_max)
  v_speed = v_speed or 1
  v_min = v_min or -FLT_MAX
  v_max = v_max or FLT_MAX

  local value_float = ffi.new("float[1]")
  value_float[0] = value

  local result = cimgui.igDragFloat(label, value_float, v_speed, v_min, v_max, "%.3f", cimgui.ImGuiSliderFlags_None)
  return value_float[0], result
end

--- Akin to :lua:meth:`drag_float`, but looks works a slider with a defined min/max
---
--- @param label string Label to show on the slider
--- @param value number Current value of the slider
--- @param v_min number Minimum slider value
--- @param v_max number Maximum slider value
---
--- @return number,boolean # The new value of the slider, and whether the slider was interacted with
function M.slider_float(label, value, v_min, v_max)
  local value_float = ffi.new("float[1]")
  value_float[0] = value

  local result = cimgui.igSliderFloat(label, value_float, v_min, v_max, "%.3f", cimgui.ImGuiSliderFlags_None)
  return value_float[0], result
end

--- Start appending to the main menu bar
---
--- @return boolean
function M.begin_main_menu_bar()
  return cimgui.igBeginMainMenuBar()
end

--- Finish appending to the main menu bar
function M.end_main_menu_bar()
  cimgui.igEndMainMenuBar()
end

--- Start appending to a menu
---
--- You can call `begin_menu()` multiple times with the same identifier to append more items to the menu.
---
--- Example:
---
--- .. code-block:: lua
---
---    if imgui.begin_menu("A menu") then
---      if imgui.menu_item("Click me") then
---        print("Clicked!")
---      end
---      imgui.end_win()
---    end
---
--- @param label string The menu string
--- @param enabled boolean|nil Whether the menu should be enabled (defaults to true)
---
--- @return boolean # Whether the menu is open
function M.begin_menu(label, enabled)
  if enabled == nil then enabled = true end
  return cimgui.igBeginMenu(label, enabled)
end

--- Finish appending to a menu
function M.end_menu()
  cimgui.igEndMenu()
end

--- Add a row to the menu
---
--- @param label string Menu item label
--- @param shortcut string|nil Text to show on the right side of the menu item. Shortcuts are just shown visually but don't actually create any keybindings you can press.
--- @param selected boolean|nil Whether to show a checkmark by the menu item (defaults to false)
--- @param enabled boolean|nil Whether to enable the menu item (defaults to true)
---
--- @return boolean # True when activated
function M.menu_item(label, shortcut, selected, enabled)
  if selected == nil then selected = false end
  if enabled == nil then enabled = true end
  return cimgui.igMenuItem_Bool(label, shortcut, selected, enabled)
end

--- A collapsable UI element
---
--- Example:
---
--- .. code-block:: lua
---
---    if imgui.tree_node("Collapsable thing") then
---      imgui.text_unformatted("I'm inside the collapsable thing")
---      imgui.tree_pop()
---    end
---
--- @param label string Label to show on the collapsable header
---
--- @return boolean # Whether the node is open (in which case, you'll also need to call :lua:meth:`tree_pop`)
function M.tree_node(label)
  return cimgui.igTreeNode_Str(label)
end

--- Finish adding to a tree node
function M.tree_pop()
  cimgui.igTreePop()
end

-- TODO: Hover flags for is_item_hovered

--- Returns if the last item is currently hovered
---
--- @return boolean
function M.is_item_hovered()
  return cimgui.igIsItemHovered(0)
end

--- Set a text-only tooptip
---
--- You usually want to combine this with :lua:meth:`is_item_hovered`
---
--- @param format string Format string passed to Lua's ``string.format``
--- @param ... any Args to pass to ``string.format``
function M.set_tooltip(format, ...)
  cimgui.igSetTooltip("%s", string.format(format, ...))
end

--- Set a text-only tooptip if the preceeding item was hovered
---
--- @param format string Format string passed to Lua's ``string.format``
--- @param ... any Args to pass to ``string.format``
function M.set_item_tooltip(format, ...)
  cimgui.igSetItemTooltip("%s", string.format(format, ...))
end

--- Draw a separator line
function M.separator()
  cimgui.igSeparator()
end

--- Draw a separator line with some text
---
--- @param label string The text to show
function M.separator_text(label)
  cimgui.igSeparatorText(label)
end

--- Draw a progress bar
---
--- TODO: Support size and overlay
---
--- @param fraction number
--- @param size nil TODO, currently does nothing
--- @param overlay string|nil Text to show on the bar
function M.progress_bar(fraction, size, overlay)
  local size = ffi.new("ImVec2") --[[@as ImVec2]]
  size.x = 0
  size.y = 0

  cimgui.igProgressBar(fraction, size, overlay)
end

return M
