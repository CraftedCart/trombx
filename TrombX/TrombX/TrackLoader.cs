using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BaboonAPI.Hooks.Tracks;
using BepInEx;
using Newtonsoft.Json;
using TrombLoader.CustomTracks;

namespace TrombX
{
    /// <summary>
    ///     Loads modchart songs from <c>CustomModChartSongs</c>
    /// </summary>
    /// <remarks>
    ///     This is very similar to TrombLoader's track loader - the main difference is just loading from a different
    ///     folder <c>CustomModchartSongs</c> instead of <c>CustomSongs</c>.
    /// </remarks>
    internal class TrackLoader : TrackRegistrationEvent.Listener
    {
        private static string CustomModchartSongsPath => Path.Combine(Paths.BepInExRootPath, "CustomModchartSongs");
        private readonly JsonSerializer _serializer = new();

        public IEnumerable<TromboneTrack> OnRegisterTracks()
        {
            // Create modchart songs dir
            if (!Directory.Exists(CustomModchartSongsPath))
            {
                Directory.CreateDirectory(CustomModchartSongsPath);
            }

            var songDirs = Directory.GetFiles(CustomModchartSongsPath, "song.tmb", SearchOption.AllDirectories)
                .Select(Path.GetDirectoryName);

            var seen = new HashSet<string>();
            foreach (var songDir in songDirs)
            {
                var chartPath = Path.Combine(songDir, "song.tmb");

                using var stream = File.OpenText(chartPath);
                using var reader = new JsonTextReader(stream);

                // Use TrombLoader's serialization types
                CustomTrackData? customLevel;
                try
                {
                    customLevel = _serializer.Deserialize<CustomTrackData>(reader);
                }
                catch (Exception exc)
                {
                    TrombXPlugin.Instance.Logger.LogWarning($"Unable to deserialize JSON of custom modchart: {chartPath}");
                    TrombXPlugin.Instance.Logger.LogWarning(exc.Message);
                    continue;
                }

                if (customLevel == null)
                {
                    TrombXPlugin.Instance.Logger.LogWarning($"Custom modchart deserialized to null: {chartPath}");
                    continue;
                }

                if (seen.Add(customLevel.trackRef))
                {
                    TrombXPlugin.Instance.Logger.LogDebug($"Found custom modchart: {customLevel.trackRef}");

                    yield return new ModTrack(songDir, customLevel);
                }
                else
                {
                    TrombXPlugin.Instance.Logger.LogWarning(
                        $"Skipping folder {chartPath} as its trackref '{customLevel.trackRef}' was already loaded!");
                }
            }
        }
    }
}
