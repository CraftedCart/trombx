trombx.mathx
============

.. contents:: :local:

Description
-----------

Math utilities that don't come as part of Lua's standard ``math`` module

API reference
-------------

.. lua:automodule:: trombx.mathx
