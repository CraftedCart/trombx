from dataclasses import dataclass
from enum import Enum
from typing import Optional

from .doc_types import Item, Field, Arg, Return


def trim_description(description: str) -> str:
    """Remove the space at the beginning of lines"""
    lines = []
    for line in description.splitlines():
        if line.startswith(" "):
            line = line[1:]
        lines.append(line)

    while len(lines) > 0 and (lines[0].isspace() or lines[0] == ""):
        del lines[0]

    while len(lines) > 0 and (lines[-1].isspace() or lines[-1] == ""):
        del lines[-1]

    return "\n".join(lines)


class Visibility(Enum):
    PUBLIC = 0
    PRIVATE = 1


@dataclass
class LuaFunctionArg:
    name: str
    type_name: str
    description: str

    @staticmethod
    def from_luals(whole_doc: list[Item], arg_doc: Arg) -> "LuaFunctionArg":
        name = "..." if arg_doc.is_vararg() else arg_doc.name

        return LuaFunctionArg(
            name=name,
            type_name=arg_doc.view,
            description=arg_doc.rawdesc,
        )


@dataclass
class LuaFunctionReturn:
    name: str
    type_name: str
    description: Optional[str]

    @staticmethod
    def from_luals(whole_doc: list[Item], arg_doc: Return) -> "LuaFunctionReturn":
        return LuaFunctionReturn(
            name=arg_doc.name,
            type_name=arg_doc.view,
            description=arg_doc.rawdesc,
        )


@dataclass
class LuaVariable:
    name: str
    type_names: list[str]
    visibility: Visibility
    description: str

    @staticmethod
    def from_luals(whole_doc: list[Item], field_doc: Field) -> "LuaVariable":
        if field_doc.extends.type == "doc.type":
            type_names = list(map(lambda ty: ty.view, field_doc.extends.types))
        else:
            type_names = [field_doc.extends.view]

        return LuaVariable(
            name=field_doc.name,
            type_names=type_names,
            visibility=Visibility.PRIVATE if field_doc.is_private() else Visibility.PUBLIC,
            description="" if field_doc.rawdesc is None else field_doc.rawdesc,
        )

    @property
    def short_description(self):
        return self.description.split("\n\n")[0]

    def is_private(self):
        return self.visibility == Visibility.PRIVATE


@dataclass
class LuaFunction:
    name: str
    visibility: Visibility
    args: list[LuaFunctionArg]
    returns: list[LuaFunctionReturn]
    description: str

    @staticmethod
    def from_luals(whole_doc: list[Item], func_doc: Field) -> "LuaFunction":
        args = []
        for arg in func_doc.extends.args:
            args.append(LuaFunctionArg.from_luals(whole_doc, arg))

        returns = []
        for ret in func_doc.extends.returns:
            returns.append(LuaFunctionReturn.from_luals(whole_doc, ret))

        return LuaFunction(
            name=func_doc.name,
            visibility=Visibility.PRIVATE if func_doc.is_private() else Visibility.PUBLIC,
            args=args,
            returns=returns,
            description="" if func_doc.rawdesc is None else trim_description(func_doc.rawdesc),
        )

    @property
    def short_description(self):
        return self.description.split("\n\n")[0]

    def is_private(self):
        return self.visibility == Visibility.PRIVATE


@dataclass
class LuaClass:
    name: str
    # parent_class_name: Optional[str]  # TODO
    description: Optional[str]
    variables: list[LuaVariable]
    functions: list[LuaFunction]

    @staticmethod
    def from_luals(whole_doc: list[Item], class_field: Field) -> "LuaClass":
        class_name = class_field.name

        # Find our class
        cls = None
        for item in whole_doc:
            if item.is_class() and item.name == class_name:
                cls = item

        if cls is None:
            raise ValueError("Class not found")

        functions = []
        variables = []
        for field in cls.fields:
            if field.is_variable():
                variables.append(LuaVariable.from_luals(whole_doc, field))
            elif field.is_function():
                functions.append(LuaFunction.from_luals(whole_doc, field))

        return LuaClass(
            name=cls.name,
            # parent_class_name=None if cls.extends is None else cls.extends.view,  # TODO
            description=None if class_field.rawdesc is None else trim_description(class_field.rawdesc),
            functions=functions,
            variables=variables,
        )

    @property
    def short_description(self) -> Optional[str]:
        if self.description is not None:
            return self.description.split("\n\n")[0]
        else:
            return None

    def has_non_private_functions(self) -> bool:
        return any(map(lambda func: func.visibility != Visibility.PRIVATE, self.functions))

    def has_non_private_variables(self) -> bool:
        return any(map(lambda var: var.visibility != Visibility.PRIVATE, self.variables))


@dataclass
class LuaReExport:
    name: str
    short_description: str

    @staticmethod
    def from_luals(whole_doc: list[Item], re_export_doc: Field) -> "LuaReExport":
        # Find our class
        cls = None
        for item in whole_doc:
            if item.is_class() and item.name == re_export_doc.name:
                cls = item

        if cls is None:
            raise ValueError("Re-export class not found")

        return LuaReExport(
            name=re_export_doc.name,
            short_description="" if cls.rawdesc is None else trim_description(cls.rawdesc).split("\n\n")[0]
        )


@dataclass
class LuaModule:
    name: str
    classes: list[LuaClass]
    variables: list[LuaVariable]
    functions: list[LuaFunction]
    re_exports: list[LuaReExport]

    @staticmethod
    def from_luals(whole_doc: list[Item], model_doc: Item) -> "LuaModule":
        classes = []
        variables = []
        functions = []
        re_exports = []
        for field in model_doc.fields:
            if field.is_class_ref():
                classes.append(LuaClass.from_luals(whole_doc, field))
            elif field.is_variable():
                variables.append(LuaVariable.from_luals(whole_doc, field))
            elif field.is_function():
                functions.append(LuaFunction.from_luals(whole_doc, field))
            elif field.is_re_export():
                re_exports.append(LuaReExport.from_luals(whole_doc, field))

        return LuaModule(
            name=model_doc.name,
            classes=classes,
            variables=variables,
            functions=functions,
            re_exports=re_exports,
        )

    def last_name_component(self) -> str:
        return self.name.split(".")[-1]

    def has_non_private_functions(self) -> bool:
        return any(map(lambda func: func.visibility != Visibility.PRIVATE, self.functions))

    def has_non_private_variables(self) -> bool:
        return any(map(lambda var: var.visibility != Visibility.PRIVATE, self.variables))
