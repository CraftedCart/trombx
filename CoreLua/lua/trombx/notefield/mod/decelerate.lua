local mod_core = require("trombx.notefield.mod.core")

local max = math.max

--- @class trombx.notefield.mod.decelerate
local M = {}

--- @class ModDecelerate : NoteModBase
--- @field public exponent number Higher numbers will cause notes to move faster before they slow down to the normal scroll speed, causing a more drastic deceleration
M.ModDecelerate = setmetatable({}, mod_core.NoteModBase)
local ModDecelerate_metatable = {
  __index = M.ModDecelerate,
  __tostring = function(self)
    return string.format("<ModDecelerate exponent=%f>", self.exponent)
  end,
}

--- Create a :lua:class:`ModDecelerate` note modifier
---
--- @param exponent number|nil Higher numbers will cause notes to move faster before they slow down to the normal scroll speed, causing a more drastic deceleration. 2 if ``nil``.
---
--- @return ModDecelerate
function M.ModDecelerate.new(exponent)
  return setmetatable(
    {
      exponent = exponent or 2,
    },
    ModDecelerate_metatable
    )
end

--- @param ctx NoteModContext
function M.ModDecelerate:modify_note(ctx)
  local beats_until_receptor = max(0, ctx.beats_until_receptor)
  ctx.position.x = ctx.position.x + ((beats_until_receptor ^ self.exponent) * ctx.notefield:get_beat_width())
end

return M
