local component = require("trombx.unity.component")
local storage = require("trombx_private.interop.storage")
local convert = require("trombx_private.interop.convert")

luanet.load_assembly("UnityEngine.CoreModule")
local UnityEngine = luanet.namespace("UnityEngine")

local UBehaviour = UnityEngine.Behaviour

--- @class trombx.unity.behaviour
local M = {}

--- Behaviours are components that can be enabled or disabled
---
--- @class Behaviour: Component
M.Behaviour = setmetatable({}, { __index = component.Component })
convert.wrapper_to_unity[M.Behaviour] = UBehaviour

--- @param lua_object Behaviour
--- @return UBehaviour
local function unwrap(lua_object)
  return storage.unwrap(lua_object) --[[@as UBehaviour]]
end

--- Set whether the behaviour should be enabled or not
---
--- Enabled behaviours are updated, disabled ones are not
---
--- @param enabled boolean Whether be behaviour should be enabled
function M.Behaviour:set_enabled(enabled)
  unwrap(self).enabled = enabled
end

--- Get whether the behaviour is be enabled or not
---
--- Enabled behaviours are updated, disabled ones are not
---
--- @return boolean
function M.Behaviour:is_enabled()
  return unwrap(self).enabled
end

--- Get whether the behaviour is be enabled and its :lua:class:`GameObject` is also active
---
--- @return boolean
function M.Behaviour:is_active_and_enabled()
  return unwrap(self).isActiveAndEnabled
end

return M
