-- Completely sandboxing Lua and LuaJIT is a tricky thing to do. We cannot guarantee 100% safety here - it's easy to
-- miss stuff in core Lua libraries and NLua's C# interop too, and LuaJIT has had exploits before to escape sandboxes -
-- but we will give it a best-effort attempt.
--
-- We do not guard against potential ways to freeze the game (infinite loops) or unbounded memory usage. Those are
-- annoyances that, while not ideal, should not cause any lasting damage to a user's computer.

-- NOTE: If updating what we do or don't sandbox, also update the sandbox docs

local SystemIO = CLRPackage("System.Runtime", "System.IO")

local path_combine = SystemIO.Path.Combine

--- @class trombx_private.sandbox
local M = {}

local dir_sep
if jit.os == "Windows" then
  dir_sep = "\\"
else
  dir_sep = "/"
end

--- Make a sandbox environment that can be passed to setfenv
function M.make_env(args)
  local require_paths = args.require.paths
  local require_passthrough_patterns = args.require.passthrough_patterns
  local require_denied_patterns = args.require.denied_patterns

  local env = {}
  env._G = env
  env._VERSION = _VERSION
  env.assert = assert
  -- collectgarbage -- Blocked: Modcharts should have no need to control the GC
  -- dofile TODO? Prevent arbitrary paths or bytecode
  env.error = error
  -- getfenv -- Blocked: Can be used to fetch an environment outside of the sandbox
  env.getmetatable = getmetatable
  env.ipairs = ipairs
  -- load TODO? Prevent bytecode
  -- loadfile TODO? Prevent arbitrary paths or bytecode
  -- loadstring TODO? Prevent bytecode
  -- module
  env.next = next
  env.pairs = pairs
  env.pcall = pcall
  env.print = print
  env.rawequal = rawequal
  env.rawget = rawget
  env.rawset = rawset
  env.require = function(mod_name)
    -- A module name should not begin with a dot or a directory separator, or be empty
    if mod_name == "" then
      error("invalid module name '" .. mod_name .. "'")
    else
      local first_char = mod_name:sub(1, 1)
      if first_char == "." or first_char == "\\" or first_char == "/" then
        error("invalid module name '" .. mod_name .. "'")
      end
    end

    -- A module name should not contain a percent sign (it would interfere with gsub)
    if mod_name:match("%%") ~= nil then
      error("invalid module name '" .. mod_name .. "'")
    end

    -- Check for denied patterns
    for _, pattern in pairs(require_denied_patterns) do
      if mod_name:match(pattern) ~= nil then
        error("cannot require module '" .. mod_name .. "' from a sandboxed environment")
      end
    end

    -- Search package.loaded first, to see if it's already loaded
    local already_loaded_mod = package.loaded[mod_name]
    if already_loaded_mod ~= nil then
      return already_loaded_mod
    end

    -- See if this is a passthrough module - if so, just use the regular Lua require func
    for _, pattern in pairs(require_passthrough_patterns) do
      if mod_name:match(pattern) ~= nil then
        return require(mod_name)
      end
    end

    local load_errors = ""

    -- Now let's have a look at loading ourself from the filesystem
    -- Replace dots with the directory separator
    local mod_relative_path = mod_name:gsub("%.", dir_sep)

    for _, require_path in ipairs(require_paths) do
      local path = require_path.path:gsub("%?", mod_relative_path)
      local func, err = loadfile(path, "t") -- Mode "t" means only load text, don't load bytecode
      if err == nil then
        -- We loaded a file, see if this should run sandboxed or not
        if require_path.priviledge == "untrusted" then
          setfenv(func, env)
        end

        -- Run the module
        local loaded_module = func()

        -- Keep track of it
        package.loaded[mod_name] = loaded_module

        -- All done
        return loaded_module
      else
        -- Failed to load the file, note its error in case every other path fails
        load_errors = load_errors .. "\n\t" .. err
      end
    end

    -- All require paths failed - throw our hands up and raise an error
    error("module '" .. mod_name .. "' not found:" .. load_errors)
  end
  env.select = select
  -- setfenv
  env.setmetatable = setmetatable
  env.tonumber = tonumber
  env.tostring = tostring
  env.type = type
  env.unpack = unpack
  env.xpcall = xpcall
  env.newproxy = newproxy -- Undocumented in Lua 5.1

  env.coroutine = {
    create = coroutine.create,
    resume = coroutine.resume,
    running = coroutine.running,
    status = coroutine.status,
    wrap = coroutine.wrap,
    yield = coroutine.yield,
    isyieldable = coroutine.isyieldable, -- LuaJIT extension from Lua 5.3
  }

  -- The debug library provides a plethora of ways to escape the sandbox. Only `debug.traceback` is allowed here.
  env.debug = {
    -- debug = debug.debug,
    -- getfenv = debug.getfenv,
    -- gethook = debug.gethook,
    -- getinfo = debug.getinfo,
    -- getlocal = debug.getlocal,
    -- getmetatable = debug.getmetatable,
    -- getregistry = debug.getregistry,
    -- getupvalue = debug.getupvalue,
    -- upvalueid = debug.upvalueid, -- LuaJIT extension
    -- upvaluejoin = debug.upvaluejoin, -- LuaJIT extension
    -- setfenv = debug.setfenv,
    -- sethook = debug.sethook,
    -- setlocal = debug.setlocal,
    -- setmetatable = debug.setmetatable,
    -- setupvalue = debug.setupvalue,
    traceback = debug.traceback,
  }

  -- We don't allow filesystem access
  -- env.io = {
  --   close = io.close,
  --   flush = io.flush,
  --   input = io.input,
  --   lines = io.lines,
  --   open = io.open,
  --   output = io.output,
  --   popen = io.popen,
  --   read = io.read,
  --   stderr = io.stderr,
  --   stdin = io.stdin,
  --   stdout = io.stdout,
  --   tmpfile = io.tmpfile,
  --   type = io.type,
  --   write = io.write,
  -- }
  -- file:close
  -- file:flush
  -- file:lines
  -- file:read
  -- file:seek
  -- file:setvbuf
  -- file:write

  env.math = {
    abs = math.abs,
    acos = math.acos,
    asin = math.asin,
    atan = math.atan,
    atan2 = math.atan2,
    ceil = math.ceil,
    cos = math.cos,
    cosh = math.cosh,
    deg = math.deg,
    exp = math.exp,
    floor = math.floor,
    fmod = math.fmod,
    frexp = math.frexp,
    huge = math.huge,
    ldexp = math.ldexp,
    log = math.log,
    log10 = math.log10,
    max = math.max,
    min = math.min,
    modf = math.modf,
    pi = math.pi,
    pow = math.pow,
    rad = math.rad,
    random = math.random,
    randomseed = math.randomseed,
    sin = math.sin,
    sinh = math.sinh,
    sqrt = math.sqrt,
    tan = math.tan,
    tanh = math.tanh,
  }

  env.os = {
    clock = os.clock,
    -- date = os.date, -- Blocked: This can crash on Lua 5.1/LuaJIT with bad format strings. This was patched in Lua
                       -- 5.2, but we're using LuaJIT here
    difftime = os.difftime,
    -- execute = os.execute, -- Blocked: This just lets you shell out to other executables
    -- exit = os.exit, -- Blocked: Modcharts should not be able to just quit the entire game
    -- getenv = os.getenv, -- Blocked: Modcharts should have no need to read off environment variables, which could
                           -- potentially reveal private info about a user's machine
    -- remove = os.remove, -- Blocked: Deletes files
    -- rename = os.rename, -- Blocked: Renames files
    -- setlocale = os.setlocale, -- Blocked: Modifying C locale can have unintended consequences to the host program
    time = os.time,
    -- tmpname = os.tmpname, -- Blocked: Unneeded without filesystem access, and may reveal info about the filesystem
                             -- structure
  }

  -- Blocked: Affects how modules are loaded
  -- env.package = {
  --   cpath = package.cpath,
  --   loaded = package.loaded, -- Blocked: Allows access to modules that the sandbox should not be able to access
  --   loaders = package.loaders,
  --   loadlib = package.loadlib, -- Blocked: Allows loading and calling C functions
  --   path = package.path,
  --   preload = package.preload,
  --   seeall = package.seeall, -- Blocked: Allows access to the global environment
  -- }

  env.string = {
    byte = string.byte,
    char = string.char,
    -- dump = string.dump, -- Blocked: Modcharts should have no need to see bytecode, esp. when bytecode loading is also
                           -- blocked
    find = string.find,
    format = string.format,
    gmatch = string.gmatch,
    gsub = string.gsub,
    len = string.len,
    lower = string.lower,
    match = string.match,
    rep = string.rep,
    reverse = string.reverse,
    sub = string.sub,
    upper = string.upper,
  }

  env.table = {
    concat = table.concat,
    insert = table.insert,
    maxn = table.maxn,
    remove = table.remove,
    sort = table.sort,
    move = table.move, -- LuaJIT extension from Lua 5.3
  }

  -- env.jit = { ... } -- Blocked: Modcharts should have no need to control the JIT
  -- env.ffi = { ... } -- Blocked: Modcharts should not be able to call into C

  -- NLua builtins are blocked since they could allow completely unrestricted access to C#/CLR code
  -- env.luanet = { ... }
  -- env.import = import
  -- env.CLRPackage = CLRPackage

  return env
end

return M
