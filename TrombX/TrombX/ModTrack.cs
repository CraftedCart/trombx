using System.IO;
using BaboonAPI.Hooks.Tracks;
using BepInEx;
using TrombLoader.CustomTracks;
using TrombLoader.CustomTracks.Backgrounds;
using UnityEngine;

namespace TrombX
{
    public class ModTrack : TromboneTrack
    {
        internal readonly string Directory;
        internal readonly CustomTrackData Data;

        public string trackref => Data.trackRef;
        public string trackname_long => Data.name;
        public string trackname_short => Data.shortName;
        public string year => Data.year.ToString();
        public string artist => Data.author;
        public string desc => Data.description;
        public string genre => Data.genre;
        public int difficulty => Data.difficulty;
        public int tempo => (int)Data.tempo;
        public int length => Mathf.FloorToInt(Data.endpoint / (Data.tempo / 60f));

        public ModTrack(string directory, CustomTrackData data)
        {
            Directory = directory;
            Data = data;
        }

        public SavedLevel LoadChart() => Data.ToSavedLevel();

        public LoadedTromboneTrack LoadTrack()
        {
            return new LoadedModTrack(this);
        }

        public bool IsVisible() => true;
    }

    public class LoadedModTrack : LoadedTromboneTrack, PauseAware
    {
        private readonly ModTrack _track;
        private readonly EmptyBackground _bg = new();
        private readonly ScriptEngine _scriptEngine = new();

        public string trackref => _track.trackref;
        public bool CanResume => true;

        public LoadedModTrack(ModTrack track)
        {
            _track = track;
        }

        public void Dispose()
        {
            _scriptEngine.Dispose();
            ScriptEngineBridge.TrackScriptEngine = null;
            ScriptEngineBridge.ClearEvents();

            if (ScriptEngineBridge.TrombXAssets != null)
            {
                ScriptEngineBridge.TrombXAssets.Unload(true);
                ScriptEngineBridge.TrombXAssets = null;
            }

            _bg.Dispose();
        }

        public TrackAudio? LoadAudio()
        {
            var path = Path.Combine(_track.Directory, "song.ogg");
            var audioClipSync = TrombLoader.Plugin.Instance.GetAudioClipSync(path);
            while (audioClipSync.MoveNext())
            {
                switch (audioClipSync.Current)
                {
                    case AudioClip clip:
                        return new TrackAudio(clip, 1f);
                    case string message:
                        TrombXPlugin.Instance.Logger.LogError(message);
                        return null;
                }
            }

            TrombXPlugin.Instance.Logger.LogError("Failed to load audio");
            return null;
        }

        public GameObject LoadBackground(BackgroundContext ctx)
        {
            var bundlePath = Path.Combine(Paths.PluginPath, "TrombX", "trombxassets");
            ScriptEngineBridge.TrombXAssets = AssetBundle.LoadFromFile(bundlePath);
            if (ScriptEngineBridge.TrombXAssets != null)
            {
                TrombXPlugin.Instance.Logger.LogInfo("Loaded TrombX asset bundle");
            }
            else
            {
                TrombXPlugin.Instance.Logger.LogError("Failed to load TrombX asset bundle");
                // TODO: Handle errors more gracefully?
            }

            return _bg.Load(ctx);
        }

        public void SetUpBackgroundDelayed(BGController controller, GameObject bg)
        {
            _bg.SetUpBackground(controller, bg);
            ScriptEngineBridge.TrackScriptEngine = _scriptEngine;
        }

        public void OnPause(PauseContext ctx) {}
        public void OnResume(PauseContext ctx) {}
    }
}
