﻿using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Rendering;

namespace DearImGuiTest
{
    public class DearImGuiRendererTest : MonoBehaviour
    {
        public Shader shader;
        private Mesh _mesh;
        public Mesh mesh2;
        private Camera _camera;

        /// <summary>
        /// Matches the layout of ImDrawVert in ImGui
        /// (which is not the same as what you'll find in Dear ImGui repo, since we've re-ordered some for TrombX since
        /// Unity needs color to come before UVs)
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        struct ImDrawVert
        {
            public float x;
            public float y;

            public uint color;

            public float uvX;
            public float uvY;
        }

        private void Start()
        {
            var mesh = new Mesh { name = "Debug UI mesh" };

            mesh.SetVertexBufferParams(
                4,
                new VertexAttributeDescriptor(VertexAttribute.Position, VertexAttributeFormat.Float32, 2),
                new VertexAttributeDescriptor(VertexAttribute.Color, VertexAttributeFormat.UNorm8, 4),
                new VertexAttributeDescriptor(VertexAttribute.TexCoord0, VertexAttributeFormat.Float32, 2)
            );
            mesh.SetVertexBufferData(
                new ImDrawVert[]
                {
                    new ImDrawVert { x = 0f, y = 0f, color = 0xFF00FFFF, uvX = 0f, uvY = 0f },
                    new ImDrawVert { x = 20f, y = 0f, color = 0xFF00FFFF, uvX = 0f, uvY = 0f },
                    new ImDrawVert { x = 20f, y = 20f, color = 0xFF00FFFF, uvX = 0f, uvY = 0f },
                    new ImDrawVert { x = 0f, y = 20f, color = 0xFF00FFFF, uvX = 0f, uvY = 0f },
                },
                0,
                0,
                4
            );

            mesh.SetIndexBufferParams(6, IndexFormat.UInt16);
            mesh.SetIndexBufferData(
                new ushort[]
                {
                    0, 1, 2,
                    0, 2, 3,
                },
                0,
                0,
                6
            );

            mesh.subMeshCount = 1; // TODO Not sure this is needed
            mesh.SetSubMesh(0, new SubMeshDescriptor(0, 6));
            mesh.RecalculateBounds(); // TODO Probably a better way

            var commands = new CommandBuffer();
            commands.name = "TrombX DebugUiRenderer";
            commands.SetViewMatrix(Matrix4x4.identity);
            commands.SetProjectionMatrix(Matrix4x4.Ortho(
                0f,
                Screen.width,
                Screen.height,
                0f,
                -1000f,
                1000f
            ));
            // TODO: Remove dependency on custom shader?
            commands.DrawMesh(
                mesh,
                Matrix4x4.identity,
                new Material(shader)
            );

            GetComponent<Camera>().AddCommandBuffer(CameraEvent.AfterEverything, commands);
        }
    }
}
