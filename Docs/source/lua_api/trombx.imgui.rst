trombx.imgui
============

.. contents:: :local:

Description
-----------

A GUI library you can use to make debug UIs. This uses `Dear ImGui <https://github.com/ocornut/imgui>`_ under the hood,
with most functions being similarly named to that of Dear ImGui. You can find further documentation inspecting the C++
headers/source for Dear ImGui, or elsewhere online.

API reference
-------------

.. lua:automodule:: trombx.imgui
