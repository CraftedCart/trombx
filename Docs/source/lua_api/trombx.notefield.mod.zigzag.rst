trombx.notefield.mod.zigzag
===========================

.. contents:: :local:

Description
-----------

Notes will zig-zag back and forth towards (and beyond) the receptor

API reference
-------------

.. lua:automodule:: trombx.notefield.mod.zigzag
