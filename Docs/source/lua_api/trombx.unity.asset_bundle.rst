trombx.unity.asset_bundle
=========================

.. contents:: :local:

Description
-----------

Unity assets that you can load from a modchart

API reference
-------------

.. lua:automodule:: trombx.unity.asset_bundle
