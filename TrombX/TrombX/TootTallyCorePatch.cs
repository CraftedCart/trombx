using System.IO;
using BaboonAPI.Hooks.Tracks;
using HarmonyLib;
using TrombX;

namespace FallenWorld
{
    // TODO: Hopefully remove this at some point
    internal class TootTallyCorePatch
    {
        [HarmonyPatch("TootTallyCore.Utils.Helpers.SongDataHelper, TootTallyCore", "GenerateBaseTmb")]
        [HarmonyPrefix]
        static bool GenerateBaseTmb_Pre(TromboneTrack track, ref string __result)
        {
            if (track is ModTrack modTrack)
            {
                TrombXPlugin.Instance.Logger.LogInfo("TootTally requested base game .tmb file for ModTrack, fixing up!");

                var tmbPath = Path.Combine(modTrack.Directory, "song.tmb");
                __result = File.ReadAllText(tmbPath);
                return false;
            }
            else
            {
                return true;
            }
        }
    }

    // internal class TootTallyBackgroundDimPatch
    // {
    //     [HarmonyPatch("TootTallyBackgroundDim.BackgroundDimController, TootTallyBackgroundDim", "BackgroundDim")]
    //     [HarmonyPrefix]
    //     static bool BackgroundDim_Pre()
    //     {
    //         // Don't do background dim if we can't find BGCameraObj otherwise stuff breaks
    //         // Not sure why BGCameraObj isn't spawned in early enough but eh w/e
    //         return GameObject.Find("BGCameraObj") != null;
    //     }
    //
    //     // [HarmonyPatch("TootTally.BackgroundDim.BackgroundDimController, TootTally.BackgroundDim", "OnLoadAssetBundleActivateBreathMeterCanvas")]
    //     // [HarmonyPrefix]
    //     // static bool OnLoadAssetBundleActivateBreathMeterCanvas_Pre()
    //     // {
    //         // var type = Type.GetType("TootTally.BackgroundDim.BackgroundDimController, TootTally.BackgroundDim");
    //         // if (type == null) return true;
    //         //
    //         // var field = type.GetField("_currentGCInstance", BindingFlags.Static | BindingFlags.NonPublic);
    //         // if (field == null) return true;
    //         //
    //         // return field.GetValue(null) != null;
    //     // }
    // }
}
